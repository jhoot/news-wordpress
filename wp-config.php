<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'news');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'toor');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.G5E*r5gD*?ebMzxf/52USR[MSy m!!r/JC{r2:NzI,wJlYLF%9?l^Z7 Ku~>HKw');
define('SECURE_AUTH_KEY',  'z(=(6?e2Rcv.<Z<H^Wpd$?{Mr~d`=1;LIg9#Yzz$T,+]8Ny2L(! %8ygwT:8h5%_');
define('LOGGED_IN_KEY',    'oPS3S|DYz`aAMIS&o(x}codO$N^%GwEyhTvI%NUiO5/#6D,^Q5C5PV56m#:6q5/Q');
define('NONCE_KEY',        'bEJ2_Gv31ZF3Pn8s5o#FRj@iwH+|p%7U$HD/XfSwM_Ge3akN8OqxS=LG3.oL-T>k');
define('AUTH_SALT',        '=K%2FY[B57w&sW9N-o5Mh!EU^t``}s1kI+OL^..JcYaEn/p!~EIS]KDK^Na^HIn}');
define('SECURE_AUTH_SALT', 'VhPmn.8160;YbmudI@,AiBm)||8+,^tq#*TF0q%&O7o.QY/|~B!u|;  F:h1x#pP');
define('LOGGED_IN_SALT',   'N}%0BYW3}AW6OvmFqq:1QP_E(Nl5cLIkiNz8m6u?:1}vO]nnD)3lLFCclT@sAouk');
define('NONCE_SALT',       'A /qqeRKzpPc:t]g m$,i(g~d 3)pjZt5ER}]rvd+li75Fpc+~Lx{MQG</pWQnEo');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
