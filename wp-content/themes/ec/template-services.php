<?php
/*
* Template Name: Services Template
*/
?>
<?php while (have_posts()) : the_post(); ?>
  <?php #get_template_part('templates/page', 'header'); ?>
  <style> figcaption{  z-index: 1; } .outside-hover:hover .inside-hover h3, .outside-hover:hover .inside-hover div{ visibility: hidden;
    opacity: 0; }</style>
      <!-- <section class="uk-small-block bg-5">
    <div class="gridl uk-text-center">
      <div class="uk-grid uk-flex uk-flex-middle" style = "transform: rotate(1.5deg);">
        <div class="uk-width-medium-1-1">
          <h1 class = "color-white uk-display-block uk-margin-bottom-remove"><?php the_title(); ?></h1>
          <?php include 'templates/breadcrumbs.php'; ?>
        </div>
      </div>
    </div>
</section> -->
  <section class="uk-block">
    <div class="gridl uk-text-center">    
      <div class="emerald-header">
        <h4 class="color-4"><?php the_field('sub_one'); ?></h4>
        <h2><?php the_field('head_one'); ?></h2>
      </div>
    </div> 
    <div class="gridm uk-text-center">    
      <p class="uk-margin-large-bottom large"><?php the_field('text_one'); ?></p>      
      <a href="<?php the_field('button_link_one'); ?>" class="uk-display-inline-block btn-2-outline small-btn"><?php the_field('button_text_one'); ?></a>
    </div>
  </section>
  <!--
  	<section data-uk-parallax="{bg: '-200'}" class="uk-block-large uk-cover-background overlay overlay-2-dark" style="background-image: url(&quot;/wp-content/uploads/2018/01/AdobeStock_114274792.jpg&quot;); background-size: auto; background-repeat: no-repeat; background-position: 50% -42.24px;">
    <div class="gridl">
    	<div class = "uk-grid-width-medium-1-2 uk-grid uk-flex uk-flex-middle">
    		<div class = "uk-margin-bottom">
		      <div class="serene-header uk-text-left">
		        <h4 class="color-4">WELCOME HOME</h4>
		        <h2 class="color-white">Learn why serene is perfect for you</h2>
		      </div>
		      <div class="uk-width-medium-1-1">
		        <p class="color-white uk-margin-medium-bottom large">Not only does drug and alcohol detoxification benefit the addict, it also has longer reaching benefits to the community and society. According to the National Institute on Drug Abuse, substance abuse results in more than $600 billion dollars in healthcare, lost work productivity and crime. Additionally, every dollar that is spent on detoxification and treatment results in a reduction of $4 to $7 in drug-related crimes. Completing treatment as a drug and alcohol detox center can help reduce healthcare costs and costs due to crime while increasing productivity, self-worth and health.</p>
		        <a href="http://localhost:8888#" class="uk-display-inline-block btn-3-hover big-btn">FIND OUT MORE</a>
		      </div>
		    </div>
		    <div class = "uk-margin-bottom">
		 	<div class="image-overlap-container uk-container-center">
              <img class="bg-image" src="http://bhope.staging.wpengine.com//wp-content/uploads/2017/12/IMG_0124.jpg">              
          	</div>
		 </div>
		 </div>		 
    </div>
  </section> -->
  <?php if(is_page(19) ) : ?>

  <?php else: ?>
    <section class="bg-white image-blocks-section shadow-top">
    	<div class="uk-grid uk-grid-collapse uk-grid-width-medium-1-2 uk-grid-width-1-1">
      <div class="image-block">
        <div class="bg-image overlay overlay-light" style="background-image: url('<?php the_field('left_image'); ?>'); background-size: cover; width: 100%; height: 100%;">

        </div>
      </div>

      <div class="text-block uk-block uk-text-left">
        <div class="text-container">
        <div class="emerald-header uk-text-left">
              <h4 class="color-4"><?php the_field('left_sub'); ?></h4>
              <h2 class="color-2"><?php the_field('left_head'); ?></h2>
            </div>
              <p class="uk-margin-medium-bottom large"><?php the_field('left_text'); ?></p>
        </div>
      </div>
        </div>
    <div class="uk-grid uk-grid-collapse uk-grid-width-medium-1-2 uk-grid-width-1-1">
      <div class="text-block uk-block uk-text-left">
        <div class="text-container">
        <div class="emerald-header uk-text-left">
              <h4 class="color-4"><?php the_field('right_sub'); ?></h4>
              <h2 class="color-2"><?php the_field('right_head'); ?></h2>
            </div>
              <p class="uk-margin-medium-bottom large"><?php the_field('right_text'); ?></p>
        </div>
      </div>
      <div class="image-block">
        <div class="bg-image overlay overlay-light" style="background-image: url('<?php the_field('right_image'); ?>'); background-size: cover; width: 100%; height: 100%;">

        </div>
      </div>
    </div>
    
  </section>
<?php endif; ?>

<section class = "uk-block-large uk-padding-bottom-normal bg-g2">
  <div class >
    <div class = "emerald-header uk-text-center uk-margin-large-bottom">
        <h4 class = "color-4">TAKE A LOOK</h4>
        <h2 class="color-2">VIEW OUR <?php the_field('whatisit'); ?></h2>
      </div>


      <?php
        $postid = get_the_ID();
        $args = array(
            'post_type'      => 'page',
            'post_parent'  => $post->ID,
            'posts_per_page' => -1,
            'order'          => 'ASC',
            'orderby'        => 'menu_order'
         );
        $mypages = new WP_Query( $args ); ?>
          <div class="programs gridl">
            <div class="uk-grid uk-grid-collapse">
              <?php while( $mypages->have_posts() ): $mypages->the_post(); ?>
                <div class="uk-width-1-1 outer-wrapper single-program uk-width-medium-1-3" style="background-image: url('<?php echo the_post_thumbnail_url(); ?>');">
                  <a href="<?php the_permalink(); ?>">
                    <div class="inner-wrapper shadow1 uk-text-center" style="padding: 20px;">
                      <h4 class="color-3"><?php the_title(); ?></h4>
                      <p class="color-2"><?php the_excerpt(); ?></p>
                    </div>
                  </a>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>
            </div>
          </div>
    </div>
  </div>
</section>

<?php include 'templates/cta.php'; ?>
 
<?php endwhile; ?>