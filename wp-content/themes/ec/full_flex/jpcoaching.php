<section  data-uk-parallax="{bg: '-200'}" class = "uk-block-big-large uk-cover-background left-block overlay overlay-1" style = "background-image: url('/emeraldcoast/wp-content/uploads/2018/05/bg2.png');">
    <div class = "gridl uk-grid">
      <div class="app-text-container uk-width-medium-2-3 uk-width-1-1">
        <div class = "emerald-header uk-text-left">
          <h4 class = "color-1"><?php the_sub_field('image_sub'); ?></h4>
          <h2 class = "color-white"><?php the_sub_field('image_head'); ?></h2>
        </div>
        <div>
          <p class="color-white uk-margin-medium-bottom large"><?php the_sub_field('image_text'); ?></p>
          <?php if(get_sub_field('image_button_text')): ?>
            <a href="<?php the_sub_field('image_button_link'); ?>" class = "uk-display-inline-block btn-1-outline small-btn"><?php the_sub_field('image_button_text'); ?></a>
          <?php endif; ?>
        </div>
      </div>
      <div class="app-image-container uk-width-medium-1-3 uk-text-left" style="overflow: hidden; margin-top: -3.5%;">
        <img src="/emeraldcoast/wp-content/uploads/2018/06/iphone2.png" style="min-height: 575px;"/>
      </div>
    </div>
  </section>