<section class = "uk-block bg-white">
    <div class = "gridl uk-text-center">    
      <div class = "emerald-header">
        <h4 class = "color-4"><?php echo the_sub_field('sub_header'); ?></h4>
        <h2 class="color-2"><?php echo the_sub_field('header'); ?></h2>
      </div>
    </div> 
    <div class = "grids uk-text-center">    
      <p class = "uk-margin-large-bottom large"><?php echo the_sub_field('text'); ?></p>      
      <a href="<?php echo get_home_url() . the_sub_field('button_link'); ?>" class = "uk-display-inline-block btn-2-outline small-btn"><?php echo the_sub_field('button_text'); ?></a>
    </div>
  </section>
