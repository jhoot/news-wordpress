  <section class="full-width-cta uk-block">
    <div class="gridl">
      <h2 class="color-1 uk-margin-bottom"><?php echo the_sub_field('text'); ?></h2>
      <a href="tel:8504241923" class="small-btn btn-3-outline">850-424-1923</a>
    </div>
  </section>