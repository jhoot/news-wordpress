<section class="bg-white image-blocks-section">
  <div class="uk-grid uk-grid-collapse uk-grid-width-medium-1-2 uk-grid-width-1-1">
    <div class="image-block">
      <div class="bg-image overlay overlay-light" style="background-image: url('<?php echo the_sub_field('image'); ?>'); background-size: cover; width: 100%; height: 100%;">

      </div>
    </div>
    <div class="text-block uk-block uk-text-left">
      <div class="text-container">
      <div class="emerald-header uk-text-left">
            <h4 class="color-4"><?php echo the_sub_field('sub_header'); ?></h4>
            <h2 class="color-2"><?php echo the_sub_field('header'); ?></h2>
          </div>
            <p class="uk-margin-medium-bottom large"><?php echo the_sub_field('text'); ?></p>
            <!-- <a href="http://localhost:8888#" class="uk-display-inline-block btn-3-hover big-btn">FIND OUT MORE</a> -->
      </div>
    </div>
    
  </div>
</section>