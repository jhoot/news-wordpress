<?php
/**
 * Template Name: Side Bar Flex
 */
?>
<?php while (have_posts()) : the_post(); ?>
	<?php
	if( get_field('top_text_area') ){ ?>
		<p class = "large"><?php the_field('top_text_area'); ?></p>
	<?php }
	// check if the flexible content field has rows of data
	if( have_rows('side_flex') ):

	 	// loop through the rows of data
	    while ( have_rows('side_flex') ) : the_row();

			// check current row layout	        
			if( get_row_layout() == 'just_paragraphs' ): ?>
			
				<?php if( have_rows('paragraph_section') ):

					while( have_rows('paragraph_section') ): the_row(); ?>
						<div class = "uk-margin-large-bottom">
							<h2 class = "color-1 uk-margin-bottom line-under"><?php the_sub_field('title'); ?></h2>
							<p class = ""><?php the_sub_field('text'); ?></p>
						</div>

					<?php endwhile;

				endif; ?>				
			<?php elseif( get_row_layout() == 'image_paragraphs' ): ?>
				
				<?php if( have_rows('paragraph_section') ):

					while( have_rows('paragraph_section') ): the_row(); ?>
						<div class = "uk-margin-large-bottom">
							<div class = "uk-grid-large uk-grid uk-flex-middle">								
								<div class = "uk-margin-bottom uk-width-large-1-2 uk-width-1-1">
										<h2 class = "color-1 uk-margin-bottom line-under"><?php the_sub_field('title'); ?></h2>
										<p class = ""><?php the_sub_field('text'); ?></p>
								</div>				
								<div class = "uk-margin-bottom uk-width-large-1-2 uk-width-1-1">
									<div class="uk-position-relative">
										<img src="<?php the_sub_field('image'); ?>" class="uk-position-relative bx-shadow-4" style="z-index: 2;">
										<!-- <div class="border-frame border-color-3 uk-hidden-medium"></div> -->
									</div>
								</div>
							</div>
						</div>

					<?php endwhile;

				endif; ?>
			<?php elseif( get_row_layout() == 'vert_image_paragraphs' ): ?>
				
				<?php if( have_rows('paragraph_section') ):

					while( have_rows('paragraph_section') ): the_row(); ?>
						<div class = "uk-margin-large-bottom">
							<div class = "uk-grid-large uk-grid uk-flex-middle">		
								<div class = "uk-margin-bottom uk-width-large-1-1 uk-width-1-1">
									<div class="uk-position-relative">
										<img src="<?php the_sub_field('image'); ?>" class="uk-position-relative bx-shadow-4" style="z-index: 2;">
										<!-- <div class="border-frame border-color-3 uk-hidden-medium"></div> -->
									</div>
								</div>						
								<div class = "uk-margin-bottom uk-width-large-1-1 uk-width-1-1">
										<h2 class = "color-1 uk-margin-bottom line-under"><?php the_sub_field('title'); ?></h2>
										<p class = ""><?php the_sub_field('text'); ?></p>
								</div>				
								
							</div>
						</div>

					<?php endwhile;

				endif; ?>	
			<?php elseif( get_row_layout() == 'editor' ): ?>
				<?php the_sub_field('editor'); ?>
								
			<?php elseif( get_row_layout() == 'accordian' ): ?>
						<div class = "uk-margin-large-bottom">
							<h2 class = "color-1 uk-margin-bottom line-under"><?php the_sub_field('title'); ?></h2>
							<p class = ""><?php the_sub_field('text'); ?></p>
							<?php if( have_rows('facts') ): ?>
								<div class="uk-accordion" data-uk-accordion>
									<?php while( have_rows('facts') ): the_row(); ?>
										<h3 class="uk-accordion-title"><?php the_sub_field('title'); ?></h3>
				    					<div class="uk-accordion-content"><p><?php the_sub_field('description'); ?></p></div>
									<?php endwhile; ?>
								</div>
							<?php endif; ?>							
						</div>
			<?php elseif( get_row_layout() == 'two_image_paragraphs' ): ?>
						<div class = "uk-margin-large-bottom">
							<h2 class = "color-1 uk-margin-bottom line-under"><?php the_sub_field('title'); ?></h2>
							<p class = ""><?php the_sub_field('text_top'); ?></p>
							<div class="uk-text-center uk-grid uk-grid-width-large-1-2 uk-grid-width-medium-1-2 uk-grid-width-1-1">
								<div class="uk-margin-bottom">
									<img class="uk-width-1-1" src="<?php the_sub_field('image_1'); ?>">
									<h5 class="uk-text-bold"><?php the_sub_field('image_1_caption'); ?></h5>
								</div>
								<div class="uk-margin-bottom">
									<img class="uk-width-1-1" src="<?php the_sub_field('image_2'); ?>">
									<h5 class="uk-text-bold"><?php the_sub_field('image_2_caption'); ?></h5>
								</div>							
							</div>
							<p class = ""><?php the_sub_field('text_bottom'); ?></p>
						</div>

	       <?php endif;

	    endwhile;

	else :

	    // no layouts found

	endif;
	
	?>
	<!-- <div class="cta-bottom-inner" id="anch5" style="background-image:url('https://coastaldetox.com/wp-content/uploads/2017/02/cd_bg2.jpg');">
		<h2>Time to Make a Difference</h2>
		<p>Finding the right help for yourself or a loved one can be an overwhelming and stressful process. We can remove those stresses by helping you find the right rehabilitation facility. Call us now to start the road to recovery.</p>
		<a class="curved-btn font1 btn-1-hover" href="#">Learn More</a>
	</div> -->
<?php endwhile; ?>
