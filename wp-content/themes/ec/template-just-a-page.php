<?php 
/**
*
* Template Name: Just a Page Template
*
**/
?>

<?php while (have_posts()) : the_post(); ?>  

<section class="uk-block">
	<div class="uk-grid gridl">
		<div class="uk-width-medium-2-3 uk-width-1-1">
			<main>
				<?php the_content(); ?>		
			</main>
		</div>
		<div class="uk-width-medium-1-3 uk-width-1-1">
			<aside>
				<?php include 'templates/sidebar.php'; ?>
			</aside>
		</div>
	</div>
</section>

<?php endwhile; ?>