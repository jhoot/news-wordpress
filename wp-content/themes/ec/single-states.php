<?php 
/**
 * States Template
 */
?>
<style>
.uk-grid{ margin-top: 0 !important; }
</style>

<?php include 'breadcrumbs.php'; ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>   

<style>
h2{ line-height: 1 !important; margin: 0; }

ul.checklist{
    padding: 10px 0 10px 20px;  
}
ul.checklist li.check-item{
    margin-bottom: 10px;
}
ul.checklist li.check-item:before{
    font-family: 'FontAwesome';
    content: '\f00C';
    margin: 0 5px 0 -15px;
    color: #012951;
}

</style>

<?php 
            $first_char_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
            $state_title = $post->post_title;
            $index = 0;

?>          

    <div class = "uk-block">
        <div class = "gridl">         
            <div>                
                <div class = "medium-padding bg-white">
                    <div class = "uk-grid">
                        <!-- <div class = "uk-width-medium-1-4 uk-text-center">
                            <div class = "mag-glass uk-display-inline-block">
                                <img width="225px" class="state-map uk-visible-large" alt="Map of <?php echo $state_title; ?>" height="225px" src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $state_title; ?>&zoom=6&size=225x225" />
                            </div>
                        </div> -->
                        <div class = "uk-width-medium-1-1">
                            <h2><?php echo $state_title; ?> Drug & Alcohol Addiction Programs</h2>
                            <p>If you are looking for a drug and alcohol addiction treatment center in <?php echo $state_title; ?> then you are in the right place. Finding the right help for yourself or a loved one can be an overwhelming and stressful process. We can remove those stresses by helping you find the right rehabilitation facility. Deciding which program is the most comprehensive and appropriate while conveniently located in  <?php echo $state_title; ?> can seem like a daunting task at first. We know that making the decision to get sober is stressful enough. At Your First Step, our caring and patient experts can help you decide which path is right for you or a loved one. </p>
                            
                            
							<div class = "uk-display-inline-block uk-margin-top "><a class = "cta-btn blue-btn uk-margin-bottom uk-text-center" href = "#findtreatment" data-uk-smooth-scroll="{offset: 92}">Find Center By City</a><a class = "cta-btn yellow-btn uk-margin-bottom uk-text-center" style = "margin-right: 0 !important;" href = "tel:18552117837"><i class = "uk-icon-phone"></i> (855) 211-7837</a> <a style = "position: relative; top: 5px; left: 5px; font-size: 36px;" href="#call-info-modal" style="" data-uk-modal="{center:true}" class="color-blue uk-icon-question-circle"></a></div>

                            <a href="tel:+18006540987" class="clearfix bottom-fixed text-center raised-box cta-insurance">
                                <span><img src="https://www.addictions.com/wp-content/themes/add2.0/assets/cigna-color.png" alt=""> <img src="https://www.addictions.com/wp-content/themes/add2.0/assets/aetna-color.png" alt=""> <img src="https://www.addictions.com/wp-content/themes/add2.0/assets/united-health-care-color.png" alt=""> <img src="https://www.addictions.com/wp-content/themes/add2.0/assets/humana-color.png" alt=""> </span> 
                                <div style="" class="cta-btn btn-3-hover font-0 uk-display-inline-block color-white" href="tel:8888888888" title="Call Medically Assisted Hotline"><i class="uk-icon-phone color-white"></i> (888) 888-8888</div> 
                            </a>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>       
    </div> 
        <section id = "findtreatment" class = "bg-1 uk-block-medium alpha-section">
            <div class = "gridl">
                <div class = "uk-clearfix">
                    <div class = "abar-title uk-margin-bottom">
                        <h2 class = "uk-text-center color-white">Find Drug & Alcohol Rehabs In <?php echo $state_title; ?></h2>             
                    </div>
                    
                    <div>
                        <div class = "alpha-bar uk-margin-bottom uk-hidden-small">          
                            <?php foreach($first_char_array as $current_letter):
                                $state_cities = $wpdb->get_results("SELECT post_name, city FROM cities WHERE SUBSTR(post_name,1,1) = '$current_letter' AND state = '$state_title' ORDER BY city;");
                                $index++;
                                if( count($state_cities[0]) > 0 ): ?>
                                    <a href = "#<?php echo $current_letter; ?>cities" class = "alpha-letter color-blue" data-uk-smooth-scroll="{offset: 135}"><?php echo $current_letter; ?></a>
                                <?php else: ?>
                                    <span class = "alpha-letter color-grey"><?php echo $current_letter; ?></span>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <a href = "#letterdir" data-uk-smooth-scroll="{offset: 64}" style = "background: #ec465a !important;" title = "Browse Detoxes By City" class = "uk-margin-bottom uk-visible-small header-btn btn">Click To See Cities</a>
                    </div>              
                </div>
            </div>  
        </section>
        <div class = "">
        <!-- Loop Through The Cities in the State-->			
            <div class = "states-container uk-margin-large-bottom" id = "letterdir">            
                    <?php foreach($first_char_array as $current_letter):
                        $state_cities = $wpdb->get_results("SELECT post_name, city FROM cities WHERE SUBSTR(post_name,1,1) = '$current_letter' AND state = '$state_title' ORDER BY city;");
                        $index++;
                        if( count($state_cities[0]) > 0 ): ?>               
                        <div class = "single-letter uk-block-medium" id = "<?php echo $current_letter; ?>cities">
                            <div class = "uk-grid">
                                <div class = "uk-width-medium-1-5 uk-visible-large uk-text-center">
                                    <span class="letter"><?php echo $current_letter; ?></span>
                                </div>
                                <div class = "uk-width-medium-4-5 uk-width-1-1">
                                    <div class = "state-divider"></div>
                                    <ul class = "uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-3 uk-grid-width-1-2" style = "padding-left: 20px; padding-right: 20px;" id = "allCities">
                                        <?php foreach($state_cities as $current_city): ?>
                                        <li class = "state-link"><a title = "<?php echo $current_city->city ?>, <?php echo $state_title ?> Drug & Alcohol Treatment Centers" href="/treatment-centers/<?php echo $current_city->post_name?>/"><?php echo $current_city->city ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <!--<hr class="uk-article-divider">-->
                    <?php endforeach; ?>
                    <?php wp_reset_postdata();?>                
            </div>                    
        </div>
<?php endwhile; ?>


<h2>Looking For Help In <?php echo $state_title; ?>?</h2>
<p><?php echo $state_title; ?>, like the rest of the country, is facing an overwhelming opioid epidemic. At Your First Step, our experts can enable you to start your recovery from opioids or any other substance. Whether you suffer from drug addiction, alcoholism, or both, our representatives are trained to help. Call us today to take your life back and find help in <?php echo $state_title; ?>.</p>

<?php get_template_part( 'featured' );?>
