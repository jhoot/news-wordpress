<div class = "uk-block-medium">
<div class = "gridnav">
<h1 class = "light">Blog</h1>

<?php
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

        if ( have_posts() ) {
        	$counter = 0;
            // Start the Loop.      
            remove_filter('the_content', 'wpautop');      
            while ( have_posts() ) : the_post();
            	if( 1 == $paged ){

            		// If first run through
            		echo ($counter == 0) ? '<div id = "bloghero"><div class = "uk-grid uk-grid-small">' : '';

            		if( $counter == 0 ){ ?>
            			<div class = "uk-width-medium-1-2">	
            				<a class = "blog-height-full uk-position-relative blog-box uk-cover-background uk-display-block" style = "background-image: url('<?php the_post_thumbnail_url('full'); ?>');" href = "<?php the_permalink(); ?>" title = "<?php the_title(); ?>">
            					<div class = "blog-box-bottom">
		            					<h2 class = "color-white light uk-margin-bottom-remove"><?php the_title(); ?></h2>
		            					<p class = "color-white light uk-margin-top-remove"><?php echo strip_tags(substr(get_the_content(),0,120)); ?></p>
	            					</div>
            				</a>
            			</div>
            		<?php } else{ ?>
            			<?php echo ($counter == 1) ? '<div class = "uk-width-medium-1-2"><div class = "uk-grid uk-grid-small uk-grid-width-medium-1-2">' : ''; ?>

            				<div class = "uk-margin-bottom">	
	            				<a class = "blog-height-half uk-position-relative blog-box uk-cover-background uk-display-block" style = "background-image: url('<?php the_post_thumbnail_url('full'); ?>');" href = "<?php the_permalink(); ?>" title = "<?php the_title(); ?>">
	            					<div class = "blog-box-bottom">
		            					<h3 class = "color-white light uk-margin-bottom-remove"><?php the_title(); ?></h3>
		            					<p class = "color-white light uk-margin-top-remove"><?php echo strip_tags(substr(get_the_content(),0,60)); ?></p>
	            					</div>
	            				</a>
            				</div>

            			<?php echo ($counter ==4) ? '</div></div>' : ''; ?>            			
            		<?php }
            		$counter++;
            		// If last run through
            		echo ($counter == 5) ? '</div></div>' : '';

            	}

            endwhile;

                //<---YOUR PAGINATION--->   

        }else{ 

            //NO POSTS FOUND OR SOMETHING  ?>
            <div class="alert alert-warning">
			    <?php _e('Sorry, no results were found.', 'sage'); ?>
			 </div>
			 <?php get_search_form(); ?>

        <?php } 

    ?>
</div>
</div>