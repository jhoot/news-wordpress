<?php
/**
 * Template Name: Contact Template
 */
?>
<?php global $phone, $phone_stripped; ?>
<?php while (have_posts()) : the_post(); ?>
  <?php #get_template_part('templates/page', 'header'); ?>
<div>
      <div class="map-overlay" onClick="style.pointerEvents='none'">

        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3446.123749415251!2d-85.96533178452313!3d30.262055081803652!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8893f28dafcaf639%3A0x979a425b41bbf02b!2s22219+Panama+City+Beach+Pkwy%2C+Panama+City+Beach%2C+FL+32413!5e0!3m2!1sen!2sus!4v1527257876172" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->

        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13784.496749765975!2d-85.9631671!3d30.2620426!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc0795ee093fb9c05!2sEmerald+Coast+Addiction+Recovery+Center!5e0!3m2!1sen!2sus!4v1527259673786" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->

        
          <iframe width="100%" height="400" frameborder="0" style="border:0; pointer-events: none;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13784.496749765975!2d-85.9631671!3d30.2620426!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc0795ee093fb9c05!2sEmerald+Coast+Addiction+Recovery+Center!5e0!3m2!1sen!2sus!4v1527259673786" allowfullscreen="">
          </iframe>
          <script>
            $('.map-overlay').click(function () {
              $('.map-overlay iframe').css("pointer-events", "auto");
            });
          </script>  
      </div>
</div>
<div class = "uk-block">
  <div class = "gridl">
<div class = "uk-grid uk-grid-width-medium-1-2">
    <div class = "uk-margin-bottom">
        <h2 class = "color-2">Address</h2>
        <p class = "large">22219 Panama City Beach Parkway<br> Panama City Beach, FL 32413</p>
    </div>
    <div class = "uk-margin-bottom">
        <h2 class = "color-2">Contact</h2>
        <p class = "large"><b>Phone:</b> <a href="tel:8504241923">850-424-1923</a><br><b>Chat With Us:</b> <a onclick="SnapEngage.startLink();">CHAT NOW</a><br><b>Email:</b> <a href="mailto:admits@journeypure.com">admits@journeypure.com</a></p>
    </div>
    <!-- <div class = "uk-margin-bottom">
      <h2 class = "color-2">Social Media</h2>
      <div class="uk-grid uk-grid-width-medium-1-4 uk-grid-width-medium-1-4 uk-grid-width-1-2">
                <div class="uk-text-left">
                  <a href="https://www.facebook.com/DrugRehabilitation/" class="color-g uk-icon-button uk-icon-facebook"></a>
                </div>
                <div class="uk-text-left">
                  <a href="https://plus.google.com/114028469625990337052" class="color-g uk-icon-button uk-icon-google-plus"></a>
                </div>
                <div class="uk-text-left">
                  <a href="https://www.instagram.com/drug_rehabilitation/?hl=en" class="color-g uk-icon-button uk-icon-instagram"></a>
                </div>
                <div class="uk-text-left">
                  <a href="" class="color-g uk-icon-button uk-icon-youtube"></a>
                </div>
              </div>

01101000 01110100 01110100 01110000 01110011 00111010 00101111 00101111 01110111 01110111 01110111 00101110 01111001 01101111 01110101 01110100 01110101 01100010 01100101 00101110 01100011 01101111 01101101 00101111 01110111 01100001 01110100 01100011 01101000 00111111 01110110 00111101 01101110 00110011 01001100 01111010 01110010 01001100 00110101 01001000 01101100 01010000 00111000


    </div>    
 -->
</div>
</div>
</div>
<?php endwhile; ?>
