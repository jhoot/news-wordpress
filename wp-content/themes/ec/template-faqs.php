<?php
/**
 * Template Name: FAQS Template
 */
?>
<?php while (have_posts()) : the_post(); ?>
	<?php #get_template_part('templates/page', 'header'); ?>
	<section class = "uk-block pattern-1">
  	<div class = "gridm">
  		<div class="uk-text-center emerald-header">
  			<h4 class="color-4"><?php the_field('sub_header'); ?></h4>
  			<h2 class="color-2"><?php the_field('header'); ?></h2>
  		</div>
  		<div class="uk-text-center">
  			<p class="large"><?php the_field('copy'); ?></p>
  		</div>
  		<!-- <div class = "uk-text-center">
			<h2 style = "" class="uk-display-inline-block line-behind color-1 uk-margin-bottom"><?php the_field('top_title'); ?></h2>
			<div class="center-divider bg-g3 uk-margin-bottom"></div>
			<p class = "large"><?php the_field('top_text'); ?></p>								
		</div>	 -->
  	</div>
  </section>
	<section class = "bg-white" style="padding-bottom: 50px;">		
		<div class = "gridl">
			<div class = "uk-text-center">
				<?php if( have_rows('facts') ): ?>
					<div class="uk-accordion" data-uk-accordion>
						<?php while( have_rows('facts') ): the_row(); ?>
							<h3 class="uk-accordion-title"><?php the_sub_field('title'); ?></h3>
	    					<div class="uk-accordion-content"><p><?php the_sub_field('description'); ?></p></div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endwhile; ?>