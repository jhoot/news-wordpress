<?php
/**
 * Template Name: About Template
 */
?>
<style>
.iframe-wrapper{
position: relative;
width: 100%;
padding-bottom: 51%;
height: 0;
}
.iframe-wrapper iframe{
position: absolute;
width: 100%;
height: 100%;
top: 0;
left: 0;
}
.green-btn:hover{
  cursor: pointer;
}
.uk-active.green-btn {
    background-color: #65afb9!important;
}
</style>
<?php while (have_posts()) : the_post(); ?>  
  <section class="uk-small-block bg-5">
    <div class="gridl uk-text-center">
      <div class="uk-grid uk-flex uk-flex-middle" style = "transform: rotate(1.5deg);">
        <div class="uk-width-medium-1-1">
          <h1 class = "color-white uk-display-block uk-margin-bottom-remove"><?php the_title(); ?></h1>
          <?php #include 'templates/breadcrumbs.php'; ?>
        </div>
      </div>
    </div>
</section>


<section class="bg-white image-blocks-section" style="margin-top: -52px;">
  <div class="uk-grid uk-grid-collapse uk-grid-width-medium-1-2 uk-grid-width-1-1">
    <div class="text-block uk-block uk-text-left">
      <div class="text-container">
      <div class="emerald-header uk-text-left">
            <h4 class="color-4"><?php the_field('sub_one'); ?></h4>
            <h2 class="color-2"><?php the_field('head_one'); ?></h2>
          </div>
            <p class="uk-margin-medium-bottom large"><?php the_field('text_one'); ?></p>
            <a href="<?php the_field('button_link_one'); ?>" class="uk-display-inline-block btn-3-hover big-btn"><?php the_field('button_text_one'); ?></a>
      </div>
    </div>
    <div class="image-block">
      <div class="bg-image overlay overlay-light" style="background-image: url('<?php the_field('image_one'); ?>'); background-size: cover; width: 100%; height: 100%;">

      </div>
    </div>
  </div>
  <div class="uk-grid uk-grid-collapse uk-grid-width-medium-1-2 uk-grid-width-1-1">
    <div class="image-block">
      <div class="bg-image overlay overlay-light" style="background-image: url('<?php the_field('image_two'); ?>'); background-size: cover; width: 100%; height: 100%;">

      </div>
    </div>

    <div class="text-block uk-block uk-text-left">
      <div class="text-container">
      <div class="emerald-header uk-text-left">
            <h4 class="color-4"><?php the_field('sub_two'); ?></h4>
            <h2 class="color-2"><?php the_field('head_two'); ?></h2>
          </div>
            <p class="uk-margin-medium-bottom large"><?php the_field('text_two'); ?></p>
            <a href="<?php the_field('button_link_two'); ?>" class="uk-display-inline-block btn-3-hover big-btn"><?php the_field('button_text_two'); ?></a>
      </div>
    </div>
      </div>
</section>

<!-- <section class="uk-block bg-white">
    <div class="gridm">
      <div class = "uk-grid uk-grid-width-medium-1-2 uk-flex uk-flex-middle">
        <div class = "uk-margin-bottom">
          <div class="emerald-header uk-text-left">
            <h4 class="color-4">LEARN WHY</h4>
            <h2 class="">Emerald Coast is Perfect For You</h2>
          </div>
            <p class="uk-margin-medium-bottom large">Not only does drug and alcohol detoxification benefit the addict, it also has longer reaching benefits to the community and society. According to the National Institute on Drug Abuse, substance abuse results in more than $600 billion dollars in healthcare, lost work productivity and crime. Additionally, every dollar that is spent on detoxification and treatment results in a reduction of $4 to $7 in drug-related crimes. Completing treatment as a drug and alcohol detox center can help reduce healthcare costs and costs due to crime while increasing productivity, self-worth and health.</p>
            <a href="http://localhost:8888#" class="uk-display-inline-block btn-3-hover big-btn">FIND OUT MORE</a>
        </div>
        <div class="bg-image-container">
          <div class="bg-image" style="background-image: url('http://bhope.staging.wpengine.com//wp-content/uploads/2017/12/IMG_0124.jpg'); ; background-size: cover;">
        </div>
        <div class = "uk-margin-bottom">
          <div class="serene-header uk-text-left">
            <h4 class="color-4">WELCOME HOME</h4>
            <h2 class="">Learn why serene is perfect for you</h2>
          </div>
            <p class="uk-margin-medium-bottom large">Not only does drug and alcohol detoxification benefit the addict, it also has longer reaching benefits to the community and society. According to the National Institute on Drug Abuse, substance abuse results in more than $600 billion dollars in healthcare, lost work productivity and crime. Additionally, every dollar that is spent on detoxification and treatment results in a reduction of $4 to $7 in drug-related crimes. Completing treatment as a drug and alcohol detox center can help reduce healthcare costs and costs due to crime while increasing productivity, self-worth and health.</p>
            <a href="http://localhost:8888#" class="uk-display-inline-block btn-3-hover big-btn">FIND OUT MORE</a>
        </div>        
      </div>  
    </div>-->  
<section class="full-width-cta uk-block">
  <div class="gridl">
    <h2 class="color-1 uk-margin-bottom">Call us today to learn about how we can help</h2>
    <a href="tel:8504241923" class="small-btn btn-3-outline">850-424-1923</a>
  </div>
</section>
</section>
  <!-- Top Slider Section -->
  <!-- <section id = "ch0" data-uk-slideshow="{kenburns:true, autoplay:true}" class = "overlay overlay-1-dark uk-position-relative">
      <div class="uk-slidenav-position">
        <?php if( have_rows('slider_images') ): ?>
          <ul class="uk-slideshow uk-slideshow-fullscreen" style = "z-index: -2;">
            <?php while(have_rows('slider_images') ): the_row(); ?>
                <li data-slideshow-slide="img">
                      <div class="uk-cover-background uk-position-cover uk-animation-scale uk-animation-reverse uk-animation-top-right" style="background-image: url(<?php echo get_img_array(get_sub_field('image'),'wide'); ?>); animation-duration: 15s;"></div>
                       the_row();</li>
              <?php endwhile; ?>
          </ul>
        <?php endif; ?>
          <div class="test-text uk-position-absolute uk-block-large" style = "top: 50vh; margin-top: -190px; padding: 0; width: 100%;">
              <div class = "gridm">
                  <div class = "uk-text-center">  
                      <h1 class = "light color-white"><?php the_field('top_title'); ?></h1>
                      <p class = "color-white large uk-text-center uk-margin-bottom"><?php the_field('top_text'); ?></p>
                      <a  data-uk-smooth-scroll=="{offset: 98}" class = "uk-margin uk-margin-right uk-margin-left uk-display-inline-block btn-white-outline big-btn" href = "#ch1">OUR MISSION</a>
                      <a  data-uk-smooth-scroll=="{offset: 98}" class = "uk-margin uk-margin-right uk-margin-left uk-display-inline-block btn-white-outline big-btn" href = "#ch2">OUR STAFF</a>
                      <a data-uk-smooth-scroll=="{offset: 98}" class = "uk-margin uk-margin-right uk-margin-left uk-display-inline-block btn-white-outline big-btn" href = "#ch3">QUICK FAQS</a>
                      <a data-uk-smooth-scroll=="{offset: 98}"  class="main-btn btn-green uk-display-inline-block btn h-btn-blue" href="#ch0"><?php the_field('top_button_text'); ?></a>
                  </div>
              </div>
          </div>
      </div>
      <a class = "uk-icon-chevron-down uk-position-absolute uk-display-block color-white" data-uk-smooth-scroll = "{offset: 77}" style = "font-size: 40px; bottom: 50px; left: 50%; margin-left: -20px; animation: bounce 2s infinite; -webkit-animation: bounce 2s infinite;" href = "#ch1"></a>
  </section> -->

  <!-- Second Section -->


  <!-- Third Section -->
  <section id = "ch1" class = "uk-block-large bg-white" style = "background-attachment: fixed; background-image: url('<?php echo get_img_array(get_field('background_image'),'wide'); ?>');">
    <div class = "gridm">
      <div class = "uk-width-1-1 uk-text-center">
         <div class="emerald-header">
        <h4 class="color-4">What You Need To Know</h4>
        <h2 class = "color-2">FAQS</h2>
      </div>
         <!-- <p class = "uk-margin-large-bottom large color-white"><?php the_field('parallax_text'); ?></p> -->
         <?php if( have_rows('faqs') ): ?>
          <div class="uk-accordion" data-uk-accordion>
            <?php while( have_rows('faqs') ): the_row(); ?>
              <h3 class="uk-accordion-title"><?php the_sub_field('title'); ?></h3>
                <div class="uk-accordion-content"><p><?php the_sub_field('text'); ?></p></div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>

<!-- 
  <section class = "uk-block-large" id = "ch0">
    <div class = "gridl uk-text-center">
      <h2 class = "line-above"><?php the_field('second_title'); ?></h2>
      <p class = "uk-margin-large-bottom large"><?php the_field('second_text'); ?></p>
      <div class = "uk-grid uk-grid-width-medium-1-2">
        <div class = "uk-margin-bottom">
            <h3><?php the_field('left_title'); ?></h3>
            <img src = "<?php echo get_img_array(get_field('left_image'),'medium_large'); ?>" />
            <p><?php the_field('left_text'); ?></p>
        </div>
        <div class = "uk-margin-bottom">
            <h3><?php the_field('right_title'); ?></h3>
            <img src = "<?php echo get_img_array(get_field('right_image'),'medium_large'); ?>" />
            <p><?php the_field('right_text'); ?></p>
        </div>
      </div>     
      <a class = "uk-margin-large-top uk-display-inline-block btn-1-outline big-btn" href = "<?php the_field('button_link'); ?>"><?php the_field('button_text'); ?></a>
    </div>
  </section> -->
<!--  <section class = "uk-block-large uk-padding-bottom-normal bg-white">
   <div class = "gridm">
    <div class = "serene-header uk-text-center uk-margin-large-bottom">
        <h4 class = "color-4">TAKE A LOOK</h4>
        <h2 >VIEW OUR PROGRAMS</h2>
      </div>
     
    <div data-uk-slider="{center:true, activecls:uk-active}">


      <?php
        $postid = get_the_ID();
        $args = array(
            'post_type'      => 'page',
            'post_parent'  => 70,
            'posts_per_page' => -1,
            'order'          => 'ASC',
            'orderby'        => 'menu_order'
         );
        $mypages = new WP_Query( $args ); ?>
        
        <div class="" data-uk-slideset="{small: 1, medium: 2, large: 3, autoplay: true}">
            <div class = "uk-slidenav-position">
              <div class = "slider-shadow">
                <ul class="uk-grid uk-grid-small uk-slideset ">
                    <?php while( $mypages->have_posts() ): $mypages->the_post(); ?>
                      <li>
                        <div class = "slider-page-item uk-text-center ">
                          <?php the_post_thumbnail(); ?>
                          <a class = "uk-display-inline-block uk-margin-top uk-margin-bottom-remove" href = "<?php the_permalink(); ?>" title = "<?php the_title(); ?>"><h4 class = "uk-text-center uk-margin-bottom-remove"><?php the_title(); ?></h4></a>
                          <p>This is a little excerpt of text that explains what the post is about</p>
                          <a href = "<?php the_permalink(); ?>" class = "uk-display-inline-block btn-1-hover small-btn" title = "<?php the_title(); ?>">LEARN MORE</a>
                        </div>
                      </li>      
                    <?php endwhile; wp_reset_postdata(); ?>
                </ul>
              </div>
              <ul class="uk-slideset-nav uk-dotnav uk-flex-center small-padding">
                  <?php while( $mypages->have_posts() ): $mypages->the_post(); ?>
                      <li><a href=""></a></li>
                  <?php endwhile; wp_reset_postdata(); ?>
              </ul>
              <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
              <a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
            </div>
            
        </div>


</div>
<p class="uk-margin-medium-bottom uk-text-center large">Not only does drug and alcohol detoxification benefit the addict, it also has longer reaching benefits to the community and society. According to the National Institute on Drug Abuse, substance abuse results in more than $600 billion dollars in healthcare, lost work productivity and crime. Additionally, every dollar that is spent on detoxification and treatment results in a reduction of $4 to $7 in drug-related crimes. Completing treatment as a drug and alcohol detox center can help reduce healthcare costs and costs due to crime while increasing productivity, self-worth and health.</p>
  </div>
</section> -->

<section  data-uk-parallax="{bg: '-200'}" class = "uk-block-big-large uk-cover-background left-block overlay overlay-1" style = "background-image: url('/emeraldcoast/wp-content/uploads/2018/05/bg2.png');">
    <div class = "gridl uk-grid">
      <div class="text-container uk-width-medium-2-3">
        <div class = "emerald-header uk-text-left">
          <h4 class = "color-1"><?php the_field('image_sub'); ?></h4>
          <h2 class = "color-white"><?php the_field('image_head'); ?></h2>
        </div>
        <div>
          <p class="color-white uk-margin-medium-bottom large"><?php the_field('image_text'); ?></p>
          <a href="<?php the_field('image_button_link'); ?>" class = "uk-display-inline-block btn-1-outline small-btn"><?php the_field('image_button_text'); ?></a>
        </div>
      </div>
      <div class="image-container uk-width-medium-1-3 uk-text-left" style="overflow: hidden; margin-top: -3.5%;">
        <img src="/emeraldcoast/wp-content/uploads/2018/06/iphone2.png" style="min-height: 575px;"/>
      </div>
    </div>
  </section>


  <!-- Staff Section -->
  <section  id = "ch2" class = "bg-g1 uk-block">
  

  <div class="uk-slidenav-position" data-uk-slider="{autoplay: true, autoplayInterval: 3500, infinite: true}">
    <div class="emerald-header uk-text-center">
      <h4 class="color-4"><?php the_field('staff_sub'); ?></h4>
      <h2 class="color-2"><?php the_field('staff_head'); ?></h2>
    </div>
    <div class="team-text gridm uk-text-center">
      <p class="large"><?php the_field('staff_text'); ?></p>
    </div>
    <div class="uk-slider-container" style="max-height: 400px; margin-top: 50px;">
      <?php
      $args = array(
        'post_type' => 'staff',
        'numposts' => -1,
        'posts_per_page' => -1,
        'order' => 'ASC',
        );

      function wpdocs_custom_excerpt_length( $length ) {
          return 20;
      }
      add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
      function wpdocs_excerpt_more( $more ) {
          return '';
      }
    add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );
    $the_query = new WP_Query( $args );
      if( $the_query->have_posts() ): ?>
        <ul class="uk-slider uk-grid-width-small-1-1 uk-grid-width-medium-1-3 uk-grid-width-large-1-4">
          <?php while($the_query->have_posts() ): $the_query->the_post(); ?>
          <?php $the_content = get_the_content();
          $the_title = get_the_title();
          $thumb_id = get_post_thumbnail_id();
          $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
          $thumb_url = $thumb_url_array[0]; ?> 
            <li>
              <div class = "uk-margin-large-bottom uk-text-center">
                <div class = "uk-overlay uk-overlay-hover uk-width-1-1">
                  <div class = "staff-member bg-white">
                    <div class = "uk-display-block bg-img-full uk-margin-bottom" style = "height: 400px; background-image: url('<?php the_field('picture') ?>'); background-size: cover;" title = "<?php the_title(); ?>"></div>
                  </div>
                  <div class = "uk-overlay-panel uk-overlay-slide-bottom uk-text-left" style = "background: rgba(0,0,0,.4); max-height: 400px; padding: 15px;">
                    <div class="text-container" style="position: relative; margin-top: 320px;">
                      <h4 class = "color-white" ><?php the_field('name'); ?></h4>
                      <p class = "color-white large" style="margin-top: -10px;"><?php the_field('title'); ?></p>
                    </div>
                  </div> 
                </div>
              </div>
            </li>
            <?php endwhile; ?><?php wp_reset_postdata(); ?>
        </ul>
      <?php endif; ?>
    </div>

    <a href="" class="uk-slidenav uk-slidenav-previous" style="margin-top: 105px" data-uk-slider-item="previous"></a>
    <a href="" class="uk-slidenav uk-slidenav-next" style="margin-top: 105px!important;" data-uk-slider-item="next"></a>

  </div>

</section>

<!-- <section class = "uk-block bg-white">
    <div class = "gridl uk-text-center">    
      <div class = "emerald-header">
        <h4 class = "color-3">Experience</h4>
        <h2 class="color-2">The JourneyPure Difference</h2>
      </div>
    </div> 
    <div class = "grids uk-text-center">    
      <p class = "uk-margin-large-bottom large">When it comes to addiction treatment, one size does not fit all, and we are here to offer several different levels of care to cater to our patients’ needs. JourneyPure Emerald Coast is able to serve clients needing detox, residential treatment, intensive outpatient treatment and partial hospitalization.  We offer a wide variety of therapy options, including our comprehensive and holistic treatment programs that help heal patients, not only from addiction but from any underlying mental health issues. Individual, group and experiential therapies are all part of our treatment model. Utilizing this dual-diagnosis approach is paramount in in building a long-term recovery foundation.</p>      
  </section> -->

<!-- FAQS Section -->
  <!-- <section class = "uk-block-large"  id = "ch3" >
    <div class = "gridl uk-text-left">
      <div class="serene-header uk-text-center uk-margin-large-bottom">
        <h4 class="color-4">What You Need To Know</h4>
        <h2><?php the_field('faqs_title'); ?></h2>
      </div>
      <p class = "uk-margin-large-bottom large uk-text-center"><?php the_field('faqs_text'); ?></p>
      <?php if( have_rows('faqs') ): ?>
          <div class="uk-accordion" data-uk-accordion>
            <?php while( have_rows('faqs') ): the_row(); ?>
              <h3 class="uk-accordion-title"><?php the_sub_field('title'); ?></h3>
                <div class="uk-accordion-content"><p><?php the_sub_field('text'); ?></p></div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>          
    </div>
  </section> -->

<?php endwhile; ?>
