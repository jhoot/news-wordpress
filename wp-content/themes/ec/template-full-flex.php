<?php
/**
 * Template Name: Full Width Flex
 */
?>



<?php while (have_posts()) : the_post(); ?>
	<?php #get_template_part('templates/page', 'header'); ?>
	
	<?php #include 'templates/quick-links.php'; ?>

	<?php

	// check if the flexible content field has rows of data
	if( have_rows('full_flex1') ):

	 	// loop through the rows of data
	    while ( have_rows('full_flex1') ) : the_row();

			// check current row layout
	        if( get_row_layout() == 'editor' ): ?>

				<?php include 'full_flex/editor.php'; ?>	

			<?php elseif( get_row_layout() == 'cta' ): ?>

				<?php include 'full_flex/cta.php'; ?>	

			<?php elseif( get_row_layout() == 'jpcoaching' ): ?>

				<?php include 'full_flex/jpcoaching.php'; ?>				

			<?php elseif( get_row_layout() == 'image_paragraphs_left' ): ?>
				
				<?php include 'full_flex/image-paragraphs-left.php'; ?>

			<?php elseif( get_row_layout() == 'image_paragraphs_right' ): ?>
	
				<?php include 'full_flex/image-paragraphs-right.php'; ?>				

			<?php elseif( get_row_layout() == 'large_paragraph_top' ): ?>
				
				<?php include 'full_flex/large-paragraph-top.php'; ?>											

	       <?php endif;

	    endwhile;

	else :

	    // no layouts found

	endif;

	?>
<?php endwhile; ?>
