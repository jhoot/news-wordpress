<?php
/**
 * Template Name: Home Template
 */
?>

 <section class="uk-block">
   <div class="gridxl" style="margin: 0 auto">
     <div class="uk-grid">
      <?php $args = array(
        'post_type' => 'post',
        'posts_per_page' => 10,
        'category_name' => 'conservative',
      );
      $query = new WP_Query($args);
      ?>
      <?php
      if ($query->have_posts()): ?>
      <div class="uk-width-1-1 uk-width-medium-1-2">
        <h1 style="text-align: center;">CONSERVATIVE BIAS</h1>
        <?php while($query->have_posts()): $query->the_post(); ?>
            <div class="article uk-grid uk-grid-collapse">
              <div class="uk-width-1-1 uk-width-medium-1-3">
                <img src="<?php the_post_thumbnail_url(); ?>">
              </div>
              <div class="uk-width-1-1 uk-width-medium-2-3">
                <h4><?php the_title(); ?></h4>
                <p><?php the_content(); ?></p>
                <a href="#">Link to Article</a>
              </div>
            </div>
          <?php endwhile; wp_reset_postdata(); ?>
      </div>
    <?php endif;  ?>


      <?php $args = array(
        'post_type' => 'post',
        'posts_per_page' => 10,
        'category_name' => 'liberal',
      );
      $query = new WP_Query($args);
      ?>
      <?php
      if ($query->have_posts()): ?>
        <div class="uk-width-1-1 uk-width-medium-1-2">
          <h1 style="text-align: center;">LIBERAL BIAS</h1>
          <?php while($query->have_posts()): $query->the_post(); ?>
            <div class="article uk-grid uk-grid-collapse">
              <div class="uk-width-1-1 uk-width-medium-1-3">
                <img src="<?php the_post_thumbnail_url(); ?>">
              </div>
              <div class="uk-width-1-1 uk-width-medium-2-3">
                <h4><?php the_title(); ?></h4>
                <p><?php the_content(); ?></p>
                <a href="#">Link to Article</a>
              </div>
            </div>
          <?php endwhile; wp_reset_postdata(); ?>
        </div>
      <?php endif; ?>
     </div>
   </div>
 </section>
