<?php include 'templates/page-header.php'; ?>
  <section class="uk-small-block bg-5">
    <div class="gridl uk-text-center">
      <div class="uk-grid uk-flex uk-flex-middle" style = "transform: rotate(1.5deg);">
        <div class="uk-width-medium-1-1">
          <h1 class = "color-white uk-display-block uk-margin-bottom-remove">Blog</h1>
          <?php #include 'templates/breadcrumbs.php'; ?>
        </div>
      </div>
    </div>
</section>
<section class="uk-block bg-white">
    <div class="gridl">   
        <?php 
        remove_filter('the_content', 'wpautop'); ?>      
      <div class = "uk-grid uk-grid-collapse">
        <?php    
            $counter = 1;
            while ( have_posts() ) : the_post(); ?>
              <?php $counter = ($counter == 9) ? 1 : $counter; ?>
                <div class="outer-wrapper uk-width-1-1 uk-width-small-1-2 uk-width-large-1-3" style="background-image: url('<?php the_post_thumbnail_url('large'); ?>'); background-size: cover; min-height: 400px; ">
                  <a href="<?php the_permalink(); ?>" >
                    <div class="inner-wrapper shadow1 uk-text-center" style="padding: 20px;">
                      <h3 class="color-2"><?php the_title(); ?></h3>
                      <p class="color-2"><?php echo substr(get_the_excerpt(),0,450); ?>...</p>
                    </div>
                  </a>
                </div>
              <?php $counter++; ?>
          <?php endwhile; wp_reset_postdata(); ?>
        </div>
      </div>
</section>