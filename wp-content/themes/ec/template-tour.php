<?php
/**
 * Template Name: Tour Template
 */
?>
<?php while (have_posts()) : the_post(); ?>
		<!-- <div class = "uk-block">			
		<div class = "gridm">
			<h2 class = "light color-blue uk-margin-large-bottom uk-text-center">Aerial Tour</h2>
			<iframe width="100%" height = "600" src="https://www.youtube.com/embed/5jrO-RBDsBQ" frameborder="0" allowfullscreen></iframe>
			<p class = "large uk-text-center uk-margin-top">Pinnacle Recovery was founded by industry professionals with decades of combined experience in personalized addiction treatment. Located in scenic Utah, we created a peaceful refuge with a network of support for those who need to step away, focus on themselves, and truly heal from addiction.</p>
		</div>
		</div> -->
		<div class = "uk-block uk-padding-bottom-remove bg-white tour" id = "fulltour">
			<div class="emerald-header uk-text-center uk-block">
				<h4 class = "color-4 uk-text-center">Take a look</h2>
				<h2 class="color-2">View images from our facility</h4>
			</div>
		<!-- <ul id="toursfilter" style = "list-style: none;" class="uk-margin-large-bottom inline-list uk-text-center">
		    <li data-uk-filter="all" class = "uk-display-inline-block"><a class="uk-display-inline-block btn-2-hover big-btn">ALL</a></li>
		    <li data-uk-filter="treatment" class = "uk-display-inline-block"><a class="uk-display-inline-block btn-2-hover big-btn">TREATMENT</a></li>
		    <li data-uk-filter="residence" class = "uk-display-inline-block"><a class="uk-display-inline-block btn-2-hover big-btn">RESIDENTIAL</a></li>
		</ul> -->
		<?php
		$args = array(
			'post_type' => 'photo',
			'posts_per_page' => -1		
			);
		$the_query = new WP_Query( $args );
		if( $the_query->have_posts() ): ?>
			<div data-uk-grid="{gutter: 0, controls: '#toursfilter'}" class = "uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-1-1 uk-grid-collapse">
				<?php
				while( $the_query->have_posts() ): $the_query->the_post(); ?>
					<?php
					$thumb_id = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
					$full_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
					$thumb_url = $thumb_url_array[0];
					$full_url = $full_url_array[0];
					?>
					<a data-uk-filter="all, <?php the_field('tour_type'); ?>"  href="<?php echo $full_url; ?>" data-uk-lightbox="{group:'my-group'}" title="<?php echo the_title(); ?>">
						<figure class="uk-overlay uk-overlay-hover" style = "height: 0;padding-bottom: 66.649%; overflow:hidden;">
						    <img src="<?php echo $thumb_url; ?>" width="" height="" alt="">
						    <figcaption class="uk-overlay-panel uk-overlay-fade uk-overlay-background">
						    	<h3 class = "color-white uk-text-center" style = "margin-top: 25%;"><?php the_title(); ?></h3>
						    </figcaption>
						</figure>
					</a> 						
				<?php endwhile; wp_reset_postdata(); ?>			
			</div>
		<?php endif; ?>
	</div>	
<?php endwhile; ?>
