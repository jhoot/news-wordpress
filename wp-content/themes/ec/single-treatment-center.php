<?php 


/**
 * Treatment Center Template
 */

  global $post_title, $city, $state, $f_state;  
  $post_name = $post->post_name;
  $listing = $wpdb->get_results("SELECT * FROM treatment_centers WHERE post_name = '$post_name';")[0];

  // $listing_type = $listing->type;
  $listing_type = 'treatment';

  $post_title = $listing->post_title;
  $post_name_1 = $listing->name1;
  $post_name_2 = $listing->name2;  
  $street_one = $listing->street1;
  $street_two = $listing->street12;
  $zip = $listing->zip;
  $city = $listing->city;
  $state = $listing->state;
  $f_state = $state;
  $county = $listing->county;
  $phone = $listing->phone;
  $intake = $listing->intake;
  $website = $listing->website;
  $director = $listing->director;
  $latitude = $listing->latitude; 
  $longitude = $listing->longitude; 
  $email = $listing->email;
  $full_address = str_replace(" ", "+", $street_one . ' ' . $street_two . ' ' . $city . ', ' . $state . ', ' . $zip);
  $style_phone = '('.substr($phone, 0, 3).') '.substr($phone, 3, 3).'-'.substr($phone,6);

  $state_full = $listing = $wpdb->get_results("SELECT * FROM treatment_centers WHERE abbreviation = '$state';")[0]->state;

	$closest_cities_query = "SELECT city, post_name FROM cities
									ORDER BY (ACOS(SIN(RADIANS('$latitude'))*
    								SIN(RADIANS(latitude))+
    								COS(RADIANS('$latitude'))*
    								COS(RADIANS(latitude))*
    								COS(RADIANS(longitude)-
    									RADIANS('$longitude'))) * 3959) ASC LIMIT 1,8";
    global $closest_cities_listings;
    $closest_cities_listings = $wpdb->get_results($closest_cities_query);

    $treatment_data['PF'][0] = 'Primary Focus of the Treatment Center';
    $treatment_data['TC'][0] = 'Type of Treatment';
    $treatment_data['FT'][0] = 'Treatment Program Type';
    $treatment_data['TAP'][0] = 'Addiction Treatment Approaches';
    $treatment_data['SMP'][0] = 'Treatment Facility Smoking Policy ';
    $treatment_data['SET'][0] = 'Treatment Service Setting';
    $treatment_data['FOP'][0] = 'Treatment Facility Operation';
    $treatment_data['LCA'][0] = 'License/certification/accreditation';
    $treatment_data['PAY'][0] = 'Payment & Insurance Policy';
    $treatment_data['EMS'][0] = 'Emergency Mental Health Services';
    $treatment_data['PYAS'][0] = 'Financial Aid & Assistance Availability';
    $treatment_data['SG'][0] = 'Special Addiction Treatment Programs';
    $treatment_data['AS'][0] = 'Other Treatment Services';
    $treatment_data['AGE'][0] = 'Ages Accepted';
    $treatment_data['GN'][0] = 'Genders Accepted';
    $treatment_data['EXCL'][0] = 'Unique Services Offered';
    $treatment_data['SL'][0] = 'Language Services for the Disabled';
    $treatment_data['AIL'][0] = 'Alaskan Native Languages or American Indian Languages';
    $treatment_data['OL'][0] = 'Secondary Languages Offered';

    $listing_info = $wpdb->get_results("SELECT * FROM treatment_details WHERE post_name = '$post_name';")[0];
    // print_r($listing_info);
    foreach( $listing_info as $key => $value):
        if( $value == 1):
            //echo '<p>' . $key . ' ' . $value . ' ';
            $category_info = $wpdb->get_results("SELECT * FROM treatment_categories WHERE treatment_code = '$key';")[0];
            // echo '<p>' . $category_info->cat_code . ' ' . $category_info->treatment_name . '</p>';
            switch ( $category_info->cat_code ){

            case 'PF':
                $treatment_data['PF'][] = $category_info->treatment_name;
                break;
            case 'TC':
                $treatment_data['TC'][] = $category_info->treatment_name;
                break;
            case 'FT':
                $treatment_data['FT'][] = $category_info->treatment_name;
                break;
            case 'TAP':
                $treatment_data['TAP'][] = $category_info->treatment_name;
                break;
            case 'SMP':
                $treatment_data['SMP'][] = $category_info->treatment_name;
                break;
            case 'SET':
                $treatment_data['SET'][] = $category_info->treatment_name;
                break;
            case 'FOP':
                $treatment_data['FOP'][] = $category_info->treatment_name;
                break;
            case 'LCA':
                $treatment_data['LCA'][] = $category_info->treatment_name;
                break;
            case 'PAY':
                $treatment_data['PAY'][] = $category_info->treatment_name;
                break;
            case 'EMS':
                $treatment_data['EMS'][] = $category_info->treatment_name;
                break;
            case 'PYAS':
                $treatment_data['PYAS'][] = $category_info->treatment_name;
                break;
            case 'SG':
                $treatment_data['SG'][] = $category_info->treatment_name;
                break;
            case 'AS':
                $treatment_data['AS'][] = $category_info->treatment_name;
                break;
            case 'AGE':
                $treatment_data['AGE'][] = $category_info->treatment_name;
                break;
            case 'GN':
                $treatment_data['GN'][] = $category_info->treatment_name;
                break;
            case 'EXCL':
                $treatment_data['EXCL'][] = $category_info->treatment_name;
                break;
            case 'SL':
                $treatment_data['SL'][] = $category_info->treatment_name;
                break;
            case 'AIL':
                $treatment_data['AIL'][] = $category_info->treatment_name;
                break;
            case 'OL':
                $treatment_data['OL'][] = $category_info->treatment_name;
                break;
        }
        endif;                                    

    endforeach;


function get_primary_data(){
    global $listing_type, $treatment_data;
    $counter = 0;
    if($listing_type == 'treatment'){ ?>
        <div class="uk-margin-bottom uk-margin-top" style = "border-top: 1px solid #ccc; padding-top: 15px;">
            <!-- Insert About Here -->
            <h3 class="uk-text-bold color-1 uk-margin-bottom" style="">Addiction Treatment Approaches</h3>
            <div class="uk-grid">                
                 <?php foreach( $treatment_data['TAP'] as $detail ):
                    if( $treatment_data['TAP'][1] != NULL ): ?>                        
                        <?php if($counter == 0 ){
                            $counter++;
                        }else{ ?>
                            <div class="uk-width-medium-1-2">
                                <p><i style="margin-right: 5px;" class="uk-icon-check"></i><?php echo $detail; ?></p>
                            </div>
                        <?php } ?>
                    <?php endif;
                endforeach; ?>
            </div>
        </div>                    
    <?php }
}

function get_secondary_data(){
    
}

?>





<div class="">


<?php include 'templates/breadcrumbs.php'; ?>
<!-- <a class = "small-padding bg-2 uk-display-block">
    <div class = "gridl uk-text-center">
        <h4 class = "uk-margin-bottom-remove color-white uk-text-bold">Click here to claim this listing and add additional information</h4>
    </div>
</a> -->

<div class = "gridl" style = "padding-top: 0 !important;">


<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>           


<div class = "uk-block">
    <div class = "uk-grid uk-grid-small">
        <div class = "uk-width-2-3 uk-margin-bottom">
            <div class="uk-grid uk-flex uk-flex-middle">
                <div class="uk-width-medium-1-6">
                    <div class="listing-type-circle">
                    </div>
                </div>
                <div class="uk-width-medium-5-6">
                    <div class="listings-item-top">
                        <h1 class = "single-title"><?php echo $post_title; ?></h1>                   
                        <span class="bottom-margin color-blue uk-display-block" style="font-weight: bold;"><i class="uk-icon-map-marker color-1"></i> 4.99 miles from the center of Beechwood Village, KY</span>
                    </div>
                </div>
                <div class = "uk-width-1-1">
                    <?php get_primary_data(); ?>
                </div>
            </div>
        </div>
        <div class = "uk-width-1-3 uk-margin-bottom">
            <div class = "bg-g2">
                <div class = "single-listing-map">
                    <iframe width="100%" height="100%" frameborder="0" scrolling="no" style="border:0" src= "https://www.google.com/maps/embed/v1/place?key=AIzaSyCTe8zq6jhWkFUCShd1cMMmHGeNVCrrz_U
                    &q=<?php echo $full_address; ?>" ></iframe>                
                </div>
                <div class = "info-box-padding">
                    <?php if( $website != ''){ ?>
                        <div class = "website-row uk-position-relative">
                            <p><strong>Website:</strong><br/><a title = "<?php echo $post_title ?> Website"  target = "_blank" href="<?php echo $website ?> "><?php echo $website?></a></p>
                        </div>
                    <?php } ?>
                    <div class = "address-row uk-position-relative">
                        <p><strong>Address:</strong><br/><span itemprop = "address" itemscope itemtype = "http://schema.org/PostalAddress"><?php echo '<span itemprop = "streetAddress">' .$street_one . ' ' . $street_two . "</span><br/>" . '<span itemprop = "addressLocality">' . $city . ", " . $f_state . '</span> , ' . '<span itemprop = "postalCode">' . $zip . '</span>' ?></span> </p>
                    </div>
                    <div class = "phone-row uk-position-relative uk-margin-medium-bottom">
                        <p><strong>Phone:</strong><br/><span itemprop = "telephone"><?php echo $style_phone ?></span></p>
                    </div>                    
                    <div class="uk-text-center">
                        <a class="uk-display-inline-block f100 btn-1-hover small-btn"><i class="uk-icon-phone"></i> CALL NOW</a>
                        <a class="uk-display-inline-block f100 btn-1-outline small-btn"><i class="uk-icon-map-marker"></i> GET DIRECTIONS</a>
                    </div>
                    <p class = "uk-text-center uk-margin-bottom-remove"><a href = "#" class = "color-2 uk-text-bold">Click To Claim Listing</a></p>
                </div>
            </div>
        </div>        
        <div class = "uk-width-1-1 uk-margin-bottom">
                <div class="listing uk-grid" itemscope itemtype = "http://schema.org/Organization">
                    <style type="text/css">
                        .listing .uk-width-medium-1-2 .white-row{
                            border: 2px solid #3d5b95;
                        }
                        strong {
                            color:#252525;
                        }
                        img.list_banner {width:100%;}
                    </style>
                    <div class = "uk-width-medium-1-2">
                        <div class = "info-row white-row uk-position-relative">
                            <p><strong itemprop = "name"><?php echo $post_title ?><br/></strong><span itemprop = "description"> is a <?php echo $treatment_data['PF'][1]  . " located in " .  $city . ", " . $f_state . "." ?></span></p>
                        </div>
                    </div>
                    <div class = "uk-width-medium-1-2">
                        <div class = "white-row">
                            <div class = "address-row uk-position-relative">
                                <p><strong>Address:</strong><br/><span itemprop = "address" itemscope itemtype = "http://schema.org/PostalAddress"><?php echo '<span itemprop = "streetAddress">' .$street_one . ' ' . $street_two . "</span><br/>" . '<span itemprop = "addressLocality">' . $city . ", " . $f_state . '</span> , ' . '<span itemprop = "postalCode">' . $zip . '</span>' ?></span> </p>
                            </div>
                            <div class = "phone-row uk-position-relative">
                                <p><strong>Phone:</strong><br/><span itemprop = "telephone"><?php echo $style_phone ?></span></p>
                            </div>
                            <?php if( $website != ''){ ?>
                                <div class = "website-row uk-position-relative">
                                    <p><strong>Website:</strong><br/><a title = "<?php echo $post_title ?> Website"  target = "_blank" href="<?php echo $website ?> "><?php echo $website?></a></p>
                                </div>
                            <?php } ?>
                        </div>
                     </div>   
                </div>
        </div>
        <div id = "moreinfo" class = "uk-width-1-1 uk-margin-bottom">
            <?php            
            foreach( $treatment_data as $type ):
                if( $type[1] != NULL ):
                    echo '<div class = "uk-margin-bottom">';
                    echo '<h3 class = "uk-text-bold color-1" style = " padding: 12px;">' . $type[0] . '</h3>';
                    echo '<div class = "uk-grid">';
                    $counter = 0;
                    foreach( $type as $detail ):
                        if( $counter == 0){
                            $counter++;
                        }else{
                            echo '<div class = "uk-width-medium-1-2">';
                            echo '<p><i style = "margin-right: 5px;" class = "uk-icon-check"></i>' . $detail . '</p>';
                            echo '</div>';
                        }
                    endforeach;
                    echo '</div></div>';
                endif;
            endforeach;
            //print_r($treatment_data);
            ?>
        </div>        
    </div>
    </div>

    



<?php wp_reset_postdata();?>
    

<?php endwhile; ?>
</div>