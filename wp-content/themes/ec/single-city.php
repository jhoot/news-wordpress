<?php 
/**
 * Template Name: Cities Template
 */

?>
<style>
.uk-grid{ margin-top: 0 !important; }
</style>
<?php
    global $global_post_title, $city;	
	$post_title = $post->post_title;
	$global_post_title = $post->post_title;
    $coordinates = $wpdb->get_results("SELECT latitude, city, longitude FROM cities WHERE post_title = '$post_title';");
    $latitude = $coordinates[0]->latitude;
    $longitude = $coordinates[0]->longitude;
    $city = $coordinates[0]->city;
	$closest_cities_query = "SELECT city, post_name, state FROM cities
									ORDER BY (ACOS(SIN(RADIANS('$latitude'))*
    								SIN(RADIANS(latitude))+
    								COS(RADIANS('$latitude'))*
    								COS(RADIANS(latitude))*
    								COS(RADIANS(longitude)-
    									RADIANS('$longitude'))) * 3959) ASC LIMIT 1,8";
    $closest_cities_listings = $wpdb->get_results($closest_cities_query); ?>

	<!-- <div class = "uk-block uk-cover-background" style = "background-image: url('/wp-content/uploads/2017/07/states_bg.jpg');">
		<div class = "gridl">			
			<div>
				<h1 class = "color-white bg-blue" style = "margin-bottom: 0; padding: 20px;">Medically Assisted Treatment in <?php echo $global_post_title; ?></h1>
				<div class = "medium-padding bg-white">
					<div class = "uk-grid">
						<div class = "uk-width-medium-1-4 uk-text-center">
							<div class = "mag-glass uk-display-inline-block">
								<img width="225px" class="state-map uk-visible-large" alt="Map of <?php echo $global_post_title; ?>" height="225px" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $latitude . ',' . $longitude; ?>&amp;zoom=12&amp;size=220x220&amp;maptype=roadmap">
							</div>
						</div>
						<div class = "uk-width-medium-3-4">
							<h2><?php echo $global_post_title; ?> Medically Assisted Addiction Programs</h2>
							<p>Medically assisted treatment centers in <?php echo $global_post_title; ?> can treat a range of addictions and co-occurring disorders. Sometimes, the most effective treatment center for your needs is not located in your town. Traveling to <?php echo $global_post_title; ?> may be necessary. Treatment centers can address:</p>							
							<div><a class = "uk-margin-top cta-btn yellow-btn uk-margin-bottom uk-text-center" style = "margin-right: 0 !important; vertical-align: middle; display: inline-block;" href = "tel:18552117837"><i class = "uk-icon-phone"></i> (855) 211-7837</a> <a style = "vertical-align: middle; display: inline-block; font-size: 36px;" href="#call-info-modal" style="" data-uk-modal="{center:true}" class="color-blue uk-icon-question-circle"></a></div>
						</div>													
					</div>
				</div>					
			</div>
		</div> 
	</div>  -->
	<div class="uk-block">
		<div class = "gridl">
			<div class="uk-width-medium-1-1">
                <h2>Montana Drug &amp; Alcohol Addiction Programs</h2>
                <p class = "uk-margin-medium-bottom">If you are looking for a drug and alcohol addiction treatment center in Montana then you are in the right place. Finding the right help for yourself or a loved one can be an overwhelming and stressful process. We can remove those stresses by helping you find the right rehabilitation facility. Deciding which program is the most comprehensive and appropriate while conveniently located in  Montana can seem like a daunting task at first. We know that making the decision to get sober is stressful enough. At Your First Step, our caring and patient experts can help you decide which path is right for you or a loved one. </p>
                
                
				<!-- <div class="uk-display-inline-block uk-margin-top "><a class="cta-btn blue-btn uk-margin-bottom uk-text-center" href="#findtreatment" data-uk-smooth-scroll="{offset: 92}">Find Center By City</a><a class="cta-btn yellow-btn uk-margin-bottom uk-text-center" style="margin-right: 0 !important;" href="tel:18552117837"><i class="uk-icon-phone"></i> (855) 211-7837</a> <a style="position: relative; top: 5px; left: 5px; font-size: 36px;" href="#call-info-modal" data-uk-modal="{center:true}" class="color-blue uk-icon-question-circle"></a></div> -->

                <!-- <a href="tel:+18006540987" class="clearfix bottom-fixed text-center raised-box cta-insurance">
                    <span><img src="https://www.addictions.com/wp-content/themes/add2.0/assets/cigna-color.png" alt=""> <img src="https://www.addictions.com/wp-content/themes/add2.0/assets/aetna-color.png" alt=""> <img src="https://www.addictions.com/wp-content/themes/add2.0/assets/united-health-care-color.png" alt=""> <img src="https://www.addictions.com/wp-content/themes/add2.0/assets/humana-color.png" alt=""> </span> 
                    <div style="" class="cta-btn btn-3-hover font-0 uk-display-inline-block color-white" href="tel:8888888888" title="Call Medically Assisted Hotline"><i class="uk-icon-phone color-white"></i> (888) 888-8888</div> 
                </a> -->
            </div>
			<div class = "uk-grid uk-grid-small">
				<div class = "uk-width-medium-1-4">
					<div class = "bg-g3 text-padding uk-block-medium">
						<div class = "filter-box uk-margin-bottom">
							<h4 class = "uk-text-bold">Near By Towns</h4>					
							<?php global $current_city; 
							foreach($closest_cities_listings as $current_city): ?>
								<a class = "color-blue uk-display-block" style = "font-size: 14px;" href = "/addiction-treatment-centers/<?php echo $current_city->post_name ?>/"><?php echo $current_city->city ?> </a>
							<?php endforeach; ?>
						</div>
						<div class = "filter-box uk-margin-bottom">
							<h4 class = "uk-text-bold">Medically Assisted Treatment</h4>
							<a class = "color-blue uk-display-block" style = "font-size: 14px;" href = "/addiction-treatment-centers/<?php echo $current_city->post_name ?>/">Suboxone </a>
							<a class = "color-blue uk-display-block" style = "font-size: 14px;" href = "/addiction-treatment-centers/<?php echo $current_city->post_name ?>/">Methadone </a>
							<a class = "color-blue uk-display-block" style = "font-size: 14px;" href = "/addiction-treatment-centers/<?php echo $current_city->post_name ?>/">Vivitrol </a>
						</div>
					</div>
				</div>			
				<div class = "uk-width-medium-3-4">
					<?php
				    // an associative array containing the query var and its value
				    $params = array('setting' => 'outpatient' );

				    $location = get_query_var('setting');
				    //echo $location;
					?>
				    <!-- pass in the $params array and the URL -->
				    <!-- <a href="<?php echo add_query_arg($params, $_SERVER['REQUEST_URI'] ); ?>">My Link</a> -->

					<?php global $globalcity; $globalcity = $post_title;    

				    $x = 1; 
				    $listingsQuery = "SELECT *, ACOS( SIN( RADIANS( '$latitude' ) ) * SIN( RADIANS( latitude ) ) + COS( RADIANS( '$latitude' ) ) * COS( RADIANS( latitude ) ) * COS( RADIANS( longitude ) - RADIANS( '$longitude' ) ) ) *3959 AS distance
					    						FROM treatment_centers 
					    						ORDER BY distance ASC Limit 1, 30;";
					$listings = $wpdb->get_results($listingsQuery); 
			 
				if ($listings): ?>
					<div class = "uk-grid">
					<?php $counter = 0;
					foreach ($listings as $post):			
						$counter++;
						$post_name = $post->post_name;
						$post_title = $post->post_title;
						$post_name_1 = $post->name1;
						$post_name_2 = $post->name2;  
						$street_one = $post->street1;
						$street_two = $post->street12;
						$zip = $post->zip;
						$city = $post->city;
						$state = $post->state;
						$county = $post->county;
						$phone = $post->phone;
						$intake = $post->intake;
						$website = $post->website;
						$director = $post->director;
						$latitude = $post->latitude; 
						$longitude = $post->longitude; 
						$email = $post->email;   
						$full_address = str_replace(" ", "+", $street_one . ' ' . $street_two . ' ' . $city . ', ' . $state . ', ' . $zip);
						$style_phone = '('.substr($phone, 0, 3).') '.substr($phone, 3, 3).'-'.substr($phone,6);
					   	?>

						<div class = "uk-width-medium-1-1 uk-width-1-1 uk-margin-bottom">
							<div class="listings-container" itemscope itemtype = "http://schema.org/Organization">
								<div class = "uk-grid uk-flex uk-flex-middle">
									<div class = "uk-width-medium-1-6">
										<div class = "listing-type-circle">
										</div>
									</div>
									<div class = "uk-width-medium-3-6">
										<div class = "listings-item-top">
											<span class = "uk-display-block" style="color:#000080;"><strong><a style = "font-size: 16px;" class = "color-red" itemprop = "url" href="/treatment-center/<?php echo $post->post_name; ?>/" title = "See full listing information of <?php echo $post->post_title; ?>"><h3 class = "uk-text-bold uk-margin-bottom-remove" itemprop = "name"><?php echo $post->post_title; ?></h3></a></strong></span>
											<span class = "uk-display-block" style = "font-size: 14px; color: #868da0;" itemprop = "address" itemscope itemtype = "http://schema.org/PostalAddress"><span itemprop = "streetAddress"><i class = "uk-icon-map-marker color-1"></i> <?php echo $street_one; ?></span><span itemprop = "addressLocality"><?php echo $city.', '.$state; ?></span>, <span itemprop = "postalCode"><?php echo $zip; ?></span></span>
											<span class ="bottom-margin color-blue uk-display-block" style = "font-weight: bold;"><?php echo number_format((float)$post->distance, 2, '.', ''); ?> miles from the center of <?php echo $global_post_title; ?></span>
										</div>
									</div>
									<div class = "uk-width-medium-2-6 uk-text-center-medium uk-text-right">
										<a class="uk-display-inline-block btn-1-hover small-btn"><i class = "uk-icon-thumbs-up"></i> VIEW LISTING</a>
										<a class="uk-display-inline-block btn-1-outline small-btn"><i class = "uk-icon-paper-plane"></i> CLAIM LISTING</a>
									</div>
								</div>								
								<div>
									<ul class="listings-services">
										<?php if ($services_text2) {echo '<li class = "listing-service" itemprop = "description">'.$services_text2.'</li>';} ?>
										<?php if ($services_text7) {echo '<li class = "listing-service">'.$services_text7.'</li>';} ?>

									</ul>
								</div>
							</div>
						</div>
					<?php endforeach;  ?>
					</div>
				<?php endif;  ?>

					<?php wp_reset_postdata();?>
					<div class = "padding-small uk-clearfix uk-margin-large-bottom">
						<div class = "text-padding">
							<h2 class = "bg-blue color-white uk-margin-bottom" style="color: white; padding: 12px;">Find An Addiction Treatment Center Near <font style = "color: #fef737;"><?php echo $global_post_title; ?></font></h2>
							<div class = "uk-grid uk-grid-width-medium-1-4 uk-grid-width-1-1">
								<?php global $current_city; 
								foreach($closest_cities_listings as $current_city): ?>
									<div class = "uk-margin-bottom"><a class = "color-blue" href = "/addiction-treatment-centers/<?php echo $current_city->post_name ?>/"><i style = "margin-right: 5px;" class = "uk-icon-check"></i> <?php echo $current_city->city ?> </a></div>
								<?php endforeach; ?>
							</div>
						</div>		
					</div>
			</div>
		</div>
	</div>
</div>

