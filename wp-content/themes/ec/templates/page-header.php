<?php use Roots\Sage\Titles; ?>

<?php $background = (get_the_post_thumbnail_url('wide') == '' ) ? get_the_post_thumbnail_url() : 'http://pinnaclerecoveryut.com/wp-content/uploads/2016/11/bg2.jpg'; ?>


<?php if(is_home()): ?>
	<div class="overlay uk-block-big-large overlay-2 uk-cover-background page-header" style = "background-image: url('<?php echo $background; ?>'); background-attachment: fixed;">
		<div class="gridl uk-text-center">
			<h1 class="color-white">Blog</h1>
			<?php include 'breadcrumbs.php'; ?>
		</div>
	</div>	
<?php else: ?>

<div class="overlay uk-block-big-large overlay-2 uk-cover-background page-header" style = "background-image: url('<?php echo $background; ?>'); background-attachment: fixed;">
	<div class="gridl uk-text-center">
		<h1 class="color-white"><?php the_title(); ?></h1>
		<?php include 'breadcrumbs.php'; ?>
	</div>
</div>

<?php endif; ?>

