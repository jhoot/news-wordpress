<script src="https://cdn.rawgit.com/mattboldt/typed.js/master/js/typed.js"></script>
<script>
  document.addEventListener("DOMContentLoaded", function(){
    Typed.new("#loved-message", {
      strings: ["Get Help For..."],
      typeSpeed: 100
    });
  });
  $(document).on('click', '.go-to-id', function(event){
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
});
</script>
  <section class = "bg-white uk-block">
    <div class = "gridm">
      <div class = "uk-text-center">
       <h2 class = "uk-display-inline-block" style = "height: 42px;" id = "loved-message"></h2>
       </div>
      <div class = "loved-one-container">
      <span class = "or">or</span>
      <ul data-uk-switcher="{connect:'#loved-one-switcher', animation: 'fade'}" style = "padding: 0; margin: 0;">
        <li>
        <a href = "#loved" class = "go-to-id uk-display-block bg-white hover-container">
          <div class = "hover-inner hover-me">                
            <div class = "me"><i class = "uk-icon-user"></i> Me</div>
            <div class = "me-hover"><i class = "uk-icon-user"></i> Me</div>
          </div>
        </a>
        </li>        
         <li>
        <a href = "#loved" style = "z-index: 2;" class = "go-to-id uk-display-block bg-white hover-container"> 
          <div class = "hover-inner hover-lo" style ="position: absolute; width:100%; bottom: 0;">
            <div class = "loved-one-hover"><i class = "uk-icon-heart"></i> Loved One</div>          
            <div class = "loved-one"><i class = "uk-icon-heart"></i> Loved One</div>
          </div>
        </a>
        </li>
      </ul>
      </div>
    </div>
  </section>
  <section class = "bg-g1">
    <div class = "uk-block">
      <div class = "gridl">
        <div class = "uk-grid uk-grid-width-medium-1-2 uk-grid-width-1-1">
          <div class = "uk-margin-bottom">
            <h2>Why Choose Ohio Addiction Recovery Center?</h2>
            <p>If you or a loved one is struggling with alcohol and drug addiction, getting sober and achieving long-term recovery requires determination and desire. It is important to find an addiction recovery center in Ohio that provides the services and support that are needed for you or your loved one to achieve total freedom from addiction. Ohio Addiction Recovery Center is the premier addiction recovery center in the state that will provide you the tools needed to break free from substance abuse.</p>
            <h4 class = "uk-text-bold uk-margin-medium-bottom">Take A Look At Our Great Programs</h4>
            <div class = "fa-ul uk-grid uk-grid-width-medium-1-2">
              <div class = "uk-margin-medium-bottom"><div class = "uk-position-relative"><i class = "fa-li uk-icon-check color-1"></i> This is an example</div></div>
              <div class = "uk-margin-medium-bottom"><div class = "uk-position-relative"><i class = "fa-li uk-icon-check color-1"></i> This is an example</div></div>
              <div class = "uk-margin-medium-bottom"><div class = "uk-position-relative"><i class = "fa-li uk-icon-check color-1"></i> This is an example</div></div>
              <div class = "uk-margin-medium-bottom"><div class = "uk-position-relative"><i class = "fa-li uk-icon-check color-1"></i> This is an example</div></div>
              <div class = "uk-margin-medium-bottom"><div class = "uk-position-relative"><i class = "fa-li uk-icon-check color-1"></i> This is an example</div></div>
              <div class = "uk-margin-medium-bottom"><div class = "uk-position-relative"><i class = "fa-li uk-icon-check color-1"></i> This is an example</div></div>              
              <div class = "uk-margin-medium-bottom"><div class = "uk-position-relative"><i class = "fa-li uk-icon-check color-1"></i> This is an example</div></div>
              <div class = "uk-margin-medium-bottom"><div class = "uk-position-relative"><i class = "fa-li uk-icon-check color-1"></i> This is an example</div></div>              
            </div>
          </div>
          <div class = "uk-margin-bottom">
            <div class = "text-padding">
                <div class = "text-padding small-padding bg-white"  style = "border: 2px solid #c5c5c5;">
                    <h3 class = "color-1 uk-text-bold">Find Out More About Restore</h3>
                    <p class = "">Restore offers a safe and comfortable environment that is full of love and support, absent of judgement, guilt and shame. Our home is equipped with the latest technology in the industry, best people in the business and a level of luxury not found in other detox facilities.</p>
                    <div class = "uk-text-center"><a class = "font1 uk-margin-left rect-btn btn-1-hover" href = "tel:8888888888"><i class = "uk-icon-phone"></i> (888) 888-8888</a></div>
                    <?php echo do_shortcode('[contact-form-7 id="1653" title="New HP Form 1"]'); ?>
                </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div style = "position: relative; top: --68px;" id = "loved"></div>
  <section id = "">
    <ul id="loved-one-switcher" class="uk-switcher">
      <li>
        <div class = "small-padding bg-1">
          <div class = "gridl">
            <h2 style = "line-height: 1.1em;" class = "uk-text-center color-white uk-margin-bottom-remove">Do you need help getting sober?</h2>
          </div>
        </div>
        <div class = "uk-block uk-cover-background" style = "background-image: url('http://malverninstitute.com/wordpress/wp-content/uploads/2017/05/single.jpg');">
          <div class = "gridl">
            <div class = "uk-grid">
              <div class = "uk-width-medium-1-2">
                <?php #if( have_rows('facts') ): ?>
                  <div class="uk-accordion" data-uk-accordion>
                    <?php #while( have_rows('facts') ): the_row(); ?>
                        <h3 class="uk-accordion-title color-white">This Is An Example Thing That You Would Want to Say</h3>
                        <div class="uk-accordion-content"><p class = "">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div>
                         <h3 class="uk-accordion-title color-white">This Is An Example Thing That You Would Want to Say</h3>
                        <div class="uk-accordion-content"><p class = "">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div>
                         <h3 class="uk-accordion-title color-white">This Is An Example Thing That You Would Want to Say</h3>
                        <div class="uk-accordion-content"><p class = "">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div>
                         <h3 class="uk-accordion-title color-white">This Is An Example Thing That You Would Want to Say</h3>
                        <div class="uk-accordion-content"><p class = "">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div>
                    <?php #endwhile; ?>
                  </div>
                  <a class = "font1 cta-btn btn-1-hover" href = "tel:8888888888"><i class = "uk-icon-phone"></i> (888) 888-8888</a>
                <?php #endif; ?>
              </div>
            </div>
        </div>
      </li>
      <li>
        <div class = "small-padding bg-1">
          <div class = "gridl">
            <h2 style = "line-height: 1.1em;" class = "uk-text-center color-white uk-margin-bottom-remove">Is A Loved One Suffering?</h2>
          </div>
        </div>
        <div class = "uk-block uk-cover-background" style = "background-image: url('http://malverninstitute.com/wordpress/wp-content/uploads/2017/05/family.jpg');">
          <div class = "gridl">
            <div class = "uk-grid">
              <div class = "uk-width-medium-1-2">
                <?php #if( have_rows('facts') ): ?>
                  <div class="uk-accordion" data-uk-accordion>
                    <?php #while( have_rows('facts') ): the_row(); ?>
                        <h3 class="uk-accordion-title color-white">This Is An Example Thing That You Would Want to Say</h3>
                        <div class="uk-accordion-content"><p class = "">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div>
                         <h3 class="uk-accordion-title color-white">This Is An Example Thing That You Would Want to Say</h3>
                        <div class="uk-accordion-content"><p class = "">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div>
                         <h3 class="uk-accordion-title color-white">This Is An Example Thing That You Would Want to Say</h3>
                        <div class="uk-accordion-content"><p class = "">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div>
                         <h3 class="uk-accordion-title color-white">This Is An Example Thing That You Would Want to Say</h3>
                        <div class="uk-accordion-content"><p class = "">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div>
                    <?php #endwhile; ?>
                  </div>
                  <a class = "font1 cta-btn btn-1-hover" href = "tel:8888888888"><i class = "uk-icon-phone"></i> (888) 888-8888</a>
                <?php #endif; ?>
              </div>
            </div>
        </div>
      </li>           
    </ul>
  </section>