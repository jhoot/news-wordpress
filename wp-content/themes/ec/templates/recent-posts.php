<!-- 
<div class = "uk-grid uk-grid-medium uk-grid-width-1-1">
	<?php
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => '3'
			);
		$the_query =  new WP_Query($args);
		if( $the_query->have_posts() ):
			while( $the_query->have_posts() ): $the_query->the_post(); ?>
				<li class = "uk-margin-bottom">
					<div class = "uk-grid ">
					  	<div class = "uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-1">
					  		<?php the_post_thumbnail('large')?>
					  	</div>
					  	<div class = "uk-width-medium-2-3 uk-width-small-1-2 uk-width-1-1">
						  	<a class="entry-summary" href = "<?php the_permalink(); ?>">

						  		<h4 class = "uk-margin-top color-1"><?php the_title(); ?></h4>						  		
						  	</a>
						  	<p style = "color: #222;"><time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time></p>
						  		<p><?php echo get_the_excerpt(); ?></p>
						</div>
					</div>
				</li>
				<hr class = "uk-article-divider">
			<?php endwhile;
		endif; ?>
</div> -->
<div class = "uk-block bg-img-full blog-roll-container overlay overlay-white" style = "background-image: url('/emeraldcoast/wp-content/uploads/2018/05/bg1.jpg'); margin-top: 75px; margin-bottom: -50px;">
    <div class = "gridl">
      <div class="emerald-header uk-text-center">
        <h4 class="color-3">Most Recent</h4>
        <h2 class = "color-2">From the blog</h2>
      </div>
      <div data-uk-slider class = "uk-position-relative">
        <div class = "uk-slider-container">
          <ul class = "uk-slider uk-grid-medium uk-grid-width-medium-1-4 uk-grid-width-1-1">
            <?php
              $args = array(
                'post_type' => 'post',
                'numberposts' => '4'
                );
              $the_query =  new WP_Query($args);
              if( $the_query->have_posts() ):
                while( $the_query->have_posts() ): $the_query->the_post(); ?>
                  <li class = "uk-margin-bottom">
                    <div class = "blog-roll bg-white" style="min-height: 450px;">
                      <div class = "blog-roll-top">
                        <div class = "bg-img-full" style = "height: 300px; background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
                      </div>
                      <div class = "blog-roll-bottom text-padding uk-block-small uk-margin-medium-top uk-text-center">
                        <h3 class="uk-margin-remove-bottom"><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                        <div class = "uk-text-center">
                          <a href = "<?php the_permalink(); ?>" class = "uk-display-inline-block uk-margin-bottom btn border-btn border-btn-green">Read More</a>
                        </div>
                      </div>
                    </div>
                  </li>
                <?php endwhile;
              endif; ?>
          </ul>
        </div>
        <!-- <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slider-item="previous"></a>
            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slider-item="next"></a> -->
      </div>
    </div>
  </div>