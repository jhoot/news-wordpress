<?php global $phone, $phone_stripped; ?>
<div class="uk-block uk-cover-background" style = "background-image: url('/wp-content/uploads/2017/08/hero-banner.jpg<?php #the_field('background'); ?>');">
	  <div class = "gridl uk-text-center">
		  
		    	<h1 class = "txt-shadow uk-margin-bottom-remove color-white uk-text-bold uk-display-block uk-margin-bottom"><?= Titles\title(); ?></h1>
		    	<a class = "uk-disply-inline-block font1 btn-1-hover rect-btn" href = "tel:<?php echo $phone_stripped; ?>"><i class = "uk-icon-phone"></i> <?php echo $phone; ?></a>
	  </div>
</div>