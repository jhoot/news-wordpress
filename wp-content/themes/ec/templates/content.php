<!-- <article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>

 -->
 <article>
<div class = "uk-grid ">
	  	<div class = "uk-width-medium-1-2 uk-width-small-1-4 uk-width-1-1">
	  		<?php the_post_thumbnail('large')?>
	  	</div>
	  	<div class = "uk-width-medium-1-2 uk-width-small-3-4 uk-width-1-1">
		  	<a class="entry-summary" href = "<?php the_permalink(); ?>">

		  		<h4 class = "uk-margin-top color-1"><?php the_title(); ?></h4>						  		
		  	</a>
		  	<p style = "color: #222;"><time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time></p>
		  		<p><?php echo substr(get_the_excerpt(),0,200); ?></p>
		</div>
	</div>
</article>