<section class="uk-block uk-container-center bg-4" id="contact-banner">
	<div class="gridm">
		<div class="copy-wrapper uk-text-center grids">
			<h2 class="color-white">Get the best treatment.</h2>
			<p class="color-2 large">Fill out the form below and we will reach out to you to let you know how we can help you get your life back!</p>
		</div>
		<?php echo do_shortcode('[contact-form-7 id="17" title="Contact Banner"]'); ?>
	</div>	
</section>