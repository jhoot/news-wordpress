<section class="uk-block-big-large blurbs-block bg-g2">
    <div class="gridm uk-text-center">
      <div class="emerald-header">
        <h4 class="color-3">Learn About</h4>
        <h2 class="color-2">What Makes Emerald Coast Different</h2>
      </div>
    </div>
    <div class="gridl">
      <div class="uk-grid uk-grid-collapse">
        <div class="outer-wrapper uk-width-1-1 uk-width-medium-1-3" style="background-image: url('/emeraldcoast/wp-content/uploads/2018/05/block1.png'); min-height: 400px;">
          <a href="#">
            <div class="inner-wrapper uk-text-center shadow1" style="padding: 20px;">
              <h4 class="color-3">Clinical Programs</h4>
              <p class="color-2">JourneyPure Emerald Coast follows the medical model of addiction tailoring individualized, evidence-based treatment plans for each patient. Due to the multi-faceted nature of addiction and our application of the medical model, our highly-credentialed therapists offer a variety clinical programs that include one-on-one therapy, group therapy, spiritual therapy, and experiential therapy as well as trauma-informed, evidence-based treatment plans.</p>
            </div>
          </a>
        </div>
        <div class="outer-wrapper uk-width-1-1 uk-width-medium-1-3" style="background-image: url('/emeraldcoast/wp-content/uploads/2018/05/block2.png'); background-size: cover; min-height: 400px;">
          <a href="#">
          <div class="inner-wrapper uk-text-center shadow1" style="padding: 20px;">
            <h4 class="color-3">Family Resources</h4>
            <p class="color-2">JourneyPure Emerald Coast is committed to helping clients and their families restore broken bonds and find a common ground for healing. Addiction is often times a systemic disease affecting not only all aspects of the individual but their surroundings and relationships as well. Because family plays a huge role in recovery, we offer treatment services that address enabling, past traumas and include family counseling and experiential therapies in our treatment options.</p>
          </div>
        </a>
        </div>
        <div class="outer-wrapper uk-width-1-1 uk-width-medium-1-3" style="background-image: url('/emeraldcoast/wp-content/uploads/2018/05/block3.png'); background-size: cover; min-height: 400px;">
          <a href="#">
          <div class="inner-wrapper uk-text-center shadow1" style="padding: 20px;">
            <h4 class="color-3">Our Location</h4>
            <p class="color-2">Located in northern Florida’s panhandle, just east of Rosemary Beach and right off 30A, JourneyPure Emerald Coast offers a daily schedule that includes therapies for mind, body, and spirit. Our patients reside in iconic beach houses where our highly-credentialed staff is on hand to ensure that each patient’s individual needs are met. Once you take a virtual tour of our campus, you’ll see why we are the area’s top choice for addiction treatment.</p>
          </div>
        </a>
        </div>
      </div>
    </div>
  </section>