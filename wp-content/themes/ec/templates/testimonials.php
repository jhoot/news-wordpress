<section class = "uk-position-relative uk-block-large uk-cover-background overlay testimonials  dark-overlay uk-block-large" style = "background-attachment: fixed; background-image: url('/emeraldcoast/wp-content/uploads/2018/05/Family-Resources.jpg');">
	<div class = "gridm" data-uk-slideshow="{autoplay:true, duration: 500, autoplayInterval: 3000}">
	<?php
		$args = array(
			'post_type' => 'testimonial',
			'numposts' => -1,
			);
		$the_query = new WP_Query( $args );
		if( $the_query->have_posts() ): ?>
			<h2 class = "uk-text-center color-white">Testimonials</h2>
			<ul class = "uk-slideshow uk-grid-width-1-1">
			<?php while($the_query->have_posts() ): $the_query->the_post(); ?>
				<?php $the_content = get_the_content();
				echo '<li class = "uk-text-center color-white large">' . $the_content . '<br><br><font style = "font-style: italic; large">' . get_the_title() . '</font></li>';
			 endwhile; ?>
			</ul>
			<ul class = "uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
                <?php $index = 0; while($the_query->have_posts() ): $the_query->the_post(); ?>
                  <li data-uk-slideshow-item = "<?php echo $index; ?>"><a href = ""></a></li>
                <?php $index++; endwhile; ?>
            </ul>
		<?php endif; ?>
	</div>
</section>