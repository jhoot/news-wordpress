<?php use Roots\Sage\Titles; ?>
<h1 class="uk-margin-top-remove entry-title"><?php the_title(); ?></h1>      
<div class = "uk-margin-bottom"><?php the_post_thumbnail(); ?></div>
<?php
if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
		<?php yoast_breadcrumb('<p id="breadcrumbs" style = "color: #222;" class = "uk-display-inline-block uk-margin-bottom-remove">','</p>'); ?>						
<?php } ?>