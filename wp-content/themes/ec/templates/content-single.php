<div class = "smart_scroll_container">
<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>      
      <h1 class="uk-margin-top-remove uk-margin-bottom-remove entry-title color-2"><?php the_title(); ?></h1>      
      <h4 style = "font-weight: lighter; margin-top: 5px !important;" class = "color-5 uk-display-block uk-margin-top-remove category-title"><?php the_date('l, F j, Y'); echo ' | By ' . get_the_author(); ?></h4>      
      <div class = "uk-margin-bottom"><?php the_post_thumbnail(); ?></div>  
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>  
<?php endwhile; ?>
</div>