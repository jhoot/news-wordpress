<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap container" role="document">
      <div class="content row">
        
        <?php if (Setup\display_sidebar()) : ?>
          <?php if( 0 ){ get_template_part('templates/blog', 'header'); } ?>
          <div class = "uk-block-medium">
            <div class = "gridl">
              <div class = "uk-grid main-grid">                      
                <main class="main uk-width-medium-2-3 uk-width-1-1">
                  <?php include Wrapper\template_path(); ?>
                </main><!-- /.main -->              
                  <aside class="sidebar uk-width-medium-1-3 uk-width-1-1">
                  <?php include Wrapper\sidebar_path(); ?>
                </aside><!-- /.sidebar -->   
              </div>
            </div>
          </div>
        <?php else: ?>
          <?php if( !is_home() && !is_front_page() && !is_404()  && !is_page_template('template-contact.php') ) {  get_template_part('templates/page', 'header'); } ?>
          <main class="main uk-width-1-1">
            <?php include Wrapper\template_path(); ?>
          </main><!-- /.main -->
        <?php endif; ?>
        <!--</div> /.uk-grid -->        
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>

