<?php 
/*
* Template Name: Treatment Child
*/ ?>
<?php #get_template_part('templates/page', 'header'); ?>
<?php while (have_posts()) : the_post(); ?>
  
  <style> figcaption{  z-index: 1; } .outside-hover:hover .inside-hover h3, .outside-hover:hover .inside-hover div{ visibility: hidden;
    opacity: 0; }</style>


  <section class="bg-white image-blocks-section">
  <div class="uk-grid uk-grid-collapse uk-grid-width-medium-1-2 uk-grid-width-1-1">
    <div class="text-block uk-block uk-text-left">
      <div class="text-container">
      <div class="emerald-header uk-text-left">
            <h4 class="color-4"><?php the_field('sub_one'); ?></h4>
            <h2 class=""><?php the_field('head_one'); ?></h2>
          </div>
            <p class="uk-margin-medium-bottom large"><?php the_field('text_one'); ?></p>
            <?php if(get_field('button_text_one')): ?>
              <a href="<?php the_field('button_link_one'); ?>" class="uk-display-inline-block btn-3-hover big-btn"><?php the_field('button_text_one'); ?></a>
            <?php endif; ?> 
      </div>
    </div>
    <div class="image-block">
      <div class="bg-image overlay overlay-light" style="background-image: url('<?php the_field('image_one'); ?>'); background-size: cover; width: 100%; height: 100%;">

      </div>
    </div>
  </div>
  <div class="uk-grid uk-grid-collapse uk-grid-width-medium-1-2 uk-grid-width-1-1">
    <div class="image-block">
      <div class="bg-image overlay overlay-light" style="background-image: url('<?php the_field('image_two'); ?>'); background-size: cover; width: 100%; height: 100%;">

      </div>
    </div>

    <div class="text-block uk-block uk-text-left">
      <div class="text-container">
      <div class="emerald-header uk-text-left">
            <h4 class="color-4"><?php the_field('sub_two'); ?></h4>
            <h2 class=""><?php the_field('head_two'); ?></h2>
          </div>
            <p class="uk-margin-medium-bottom large"><?php the_field('text_two'); ?></p>
            <?php if(get_field('button_text_two')): ?>
              <a href="<?php the_field('button_link_two'); ?>" class="uk-display-inline-block btn-3-hover big-btn"><?php the_field('button_text_two'); ?></a>
            <?php endif; ?>
      </div>
    </div>
      </div>
</section>

<?php include 'templates/cta.php'; ?>

<section class="uk-block child-text">
  <div class="uk-grid gridl">
    <div class="uk-width-medium-2-3">
        <main class="container">
          <p>
            <?php the_content(); ?>
          </p>
        </main>
    </div>
    <div class="uk-width-medium-1-3">
        <aside>
          <?php include "templates/sidebar.php"; ?>
        </aside>
    </div>
  </div>
</section>


<?php endwhile; ?>