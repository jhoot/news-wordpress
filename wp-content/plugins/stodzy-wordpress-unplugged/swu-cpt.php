<?php
/*
* CPT STUFF
*/
function swu_create_staff(){
    $labels = array(
        'name' => _x('Staff', 'staff'),
        'singular_name' => _x('Staff', 'staff'),
        'add_New' => _x('New Staff', 'staff'),
        'add_New_item' => __('Add New Staff'),
        'edit_item' => __('Edit staff'),
        'New_item' => __('New staff'),
        'view_item' => __('View staff'),
        'search_items' => __('Search staff'),
        'not_found' =>  __('Staff not found.'),
        'not_found_in_trash' => __('There are no staffs in the trash.'), 
        'parent_item_colon' => '',
        'menu_name' => 'Staff'
      );
    
  register_post_type( 'staff',
    array(
      'labels' => $labels,
      'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => true,
        'menu_icon' => 'dashicons-universal-access',
        'menu_position' => null,
        'supports' => array('title','editor', 'genesis-seo' , 'genesis-layouts' , 'genesis-simple-sidebars' ,'thumbnail','custom-fields', 'page-attributes'),
    )
  );
  add_post_type_support( 'page', 'custom-fields' );
}

function swu_create_programs(){
    $labels = array(
        'name' => _x('Programs', 'programs'),
        'singular_name' => _x('Program', 'program'),
        'add_New' => _x('New Program', 'program'),
        'add_New_item' => __('Add New Program'),
        'edit_item' => __('Edit program'),
        'New_item' => __('New program'),
        'view_item' => __('View program'),
        'search_items' => __('Search programs'),
        'not_found' =>  __('Program not found.'),
        'not_found_in_trash' => __('There are no programs in the trash.'), 
        'parent_item_colon' => '',
        'menu_name' => 'Programs'
      );
    
  register_post_type( 'programs',
    array(
      'labels' => $labels,
      'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => true,
        'menu_icon' => 'dashicons-universal-access',
        'menu_position' => null,
        'supports' => array('title','editor', 'genesis-seo' , 'genesis-layouts' , 'genesis-simple-sidebars' ,'thumbnail','custom-fields', 'page-attributes'),
    )
  );
  add_post_type_support( 'page', 'custom-fields' );
}


function swu_create_testimonials(){
  $labels = array(
        'name' => _x('Testimonials', 'testimonials'),
        'singular_name' => _x('Testimonial', 'testimonial'),
        'add_New' => _x('New Testimonial', 'book'),
        'add_New_item' => __('Add New testimonial'),
        'edit_item' => __('Edit testimonial'),
        'New_item' => __('New testimonial'),
        'view_item' => __('View testimonial'),
        'search_items' => __('Search testimonials'),
        'not_found' =>  __('Testimonial not found.'),
        'not_found_in_trash' => __('There are no testimonials in the trash.'), 
        'parent_item_colon' => '',
        'menu_name' => 'Testimonials'
      );
    
  register_post_type( 'testimonial',
    array(
      'labels' => $labels,
      'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => array(
        'slug'=>'testimonial',
        'with_front'=> false),
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => true,
        'menu_position' => null,
        'menu_icon' => 'dashicons-star-filled',
  'supports' => array('title','editor','thumbnail','custom-fields'),
    )
  );
  add_post_type_support( 'page', 'custom-fields' );

}

function swu_create_images(){
  $labels = array(
        'name' => _x('Photos', 'photos'),
        'singular_name' => _x('Photo', 'photo'),
        'add_New' => _x('New Photo', 'book'),
        'add_New_item' => __('Add New photo'),
        'edit_item' => __('Edit photo'),
        'New_item' => __('New photo'),
        'view_item' => __('View photo'),
        'search_items' => __('Search photos'),
        'not_found' =>  __('Photo not found.'),
        'not_found_in_trash' => __('There are no photos in the trash.'), 
        'parent_item_colon' => '',
        'menu_name' => 'Photos'
      );
    
  register_post_type( 'photo',
    array(
      'labels' => $labels,
      'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => array(
        'slug'=>'photo',
        'with_front'=> false),
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => true,
        'menu_position' => null,
        'menu_icon' => 'dashicons-camera',
  'supports' => array('title','editor','thumbnail','custom-fields'),
    )
  );
  add_post_type_support( 'page', 'custom-fields' );
}

function swu_create_logos(){
  $labels = array(
        'name' => _x('Logos', 'logos'),
        'singular_name' => _x('Logo', 'logo'),
        'add_New' => _x('New Logo', 'book'),
        'add_New_item' => __('Add New logo'),
        'edit_item' => __('Edit logo'),
        'New_item' => __('New logo'),
        'view_item' => __('View logo'),
        'search_items' => __('Search logos'),
        'not_found' =>  __('Logo not found.'),
        'not_found_in_trash' => __('There are no logos in the trash.'), 
        'parent_item_colon' => '',
        'menu_name' => 'Logos'
      );
    
  register_post_type( 'logo',
    array(
      'labels' => $labels,
      'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => array(
        'slug'=>'logo',
        'with_front'=> false),
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => true,
        'menu_position' => null,
        'menu_icon' => 'dashicons-camera',
  'supports' => array('title','editor','thumbnail','custom-fields'),
    )
  );
  add_post_type_support( 'page', 'custom-fields' );
}